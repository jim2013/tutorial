#!/bin/bash

baseDir=/opt/v2ray

function start()
{
	if [ -f $baseDir/v2ray.pid ]; then
		echo 'found v2ray.pid, please run stop it first.'
		exit 1
	fi
	echo 'starting...'
	$baseDir/v2ray -config $baseDir/config.json > $baseDir/v2ray.log 2>&1 &
	echo $! > $baseDir/v2ray.pid
}

function stop()
{
	echo 'stopping...'
	pidfile=$baseDir/v2ray.pid
	pid=`cat $pidfile`
	if [ "$pid" == "" ]; then
		pid=`ps aux | grep v2ray | grep opt | awk '{print $2}'`
	fi
	if [ "$pid" != "" ]; then
		kill $pid
		echo 'stop v2ray, pid is '$pid
		rm $baseDir/v2ray.pid
		exit 0
	fi
	echo 'fail to stop v2ray, can not find pid'
}

function restart()
{
	stop
	start
}

if [ $# == 0 ]; then
	echo 'usage: t.sh start|stop'
	exit 1
fi

if [ $1 == 'start' ]; then
	start
elif [ $1 == 'stop' ]; then
	stop
elif [ $1 == 'restart' ]; then
	restart
else
	echo 'unknown cmd'
fi
