#!/bin/bash

url="https://tldr.ostera.io/"
if [ $# == 1 ]
then
	url=$url$1
fi
lynx ${url}
