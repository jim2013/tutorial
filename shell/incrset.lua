-- redis lua : access Keys and Arguments 
-- When writing Lua scripts for Redis, every key that is accessed should be
-- accessed only by the KEYS table. The ARGV table is used for
-- parameter-passing — here it’s the value of the URL we want to store.
--
-- run cmd:redis-cli --eval incrset.lua links:counter links:urls , http://malcolmgladwellbookgenerator.com/

local link_id = redis.call("INCR", KEYS[1])
redis.call("HSET", KEYS[2], link_id, ARGV[1])
return link_id
