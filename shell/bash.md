## string
* 字符串长度
```bash
#!/bin/bash
str="Good morning"
len=${#str}
```
* substring
```bash
#!/bin/bash
test="Welcome to the Land of Linux"
test1=${test:0:7}
```
