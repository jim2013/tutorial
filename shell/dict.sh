#!/bin/bash

url="https://dictionary.cambridge.org/dictionary/english-chinese-simplified/"
if [ $# == 1 ]
then
	url=$url$1
fi
lynx ${url}
