#!/bin/bash

curl -H'Content-Type:application/json' -XGET '192.168.1.96:9200/order_item_v2/_search?pretty' -d '
{
	"query":{
		"bool":{
				"filter":[
						{"term":{"order_type": 4}},
						{"term":{"channel":4}},
						{"term":{"status":1}}
				]
		}
	},
	"size":2
}
'
