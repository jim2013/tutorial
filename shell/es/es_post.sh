#!/bin/bash

curl -XGET '192.168.6.55:9200/group_post_v2/_search?pretty' -d '
{
  "from" : 0,
  "size" : 20,
  "timeout" : 60000,
  "query" : {
    "filtered" : {
      "query" : {
        "match_all" : { }
      },
      "filter" : {
        "bool" : {
          "must" : [ {
            "term" : {
              "theme_id" : 990
            }
          }, {
            "term" : {
              "group_status" : 1
            }
          } ],
          "should" : [ {
            "range" : {
              "hot" : {
                "from" : null,
                "to" : 0,
                "include_lower" : true,
                "include_upper" : false
              }
            }
          }, {
            "bool" : {
              "must" : [ {
                "term" : {
                  "hot" : 0
                }
              }, {
                "range" : {
                  "id" : {
                    "from" : null,
                    "to" : 7220186,
                    "include_lower" : true,
                    "include_upper" : false
                  }
                }
              } ]
            }
          }, {
            "term" : {
              "status" : 0
            }
          }, {
            "bool" : {
              "must" : {
                "term" : {
                  "user_id" : 21923451
                }
              },
              "should" : [ {
                "term" : {
                  "status" : 2
                }
              }, {
                "term" : {
                  "status" : 5
                }
              }, {
                "term" : {
                  "status" : 1
                }
              } ]
            }
          } ]
        }
      }
    }
  },
  "_source" : false,
  "sort" : [ {
    "hot" : {
      "order" : "desc"
    }
  }, {
    "id" : {
      "order" : "desc"
    }
  } ]
}
'
#pesudo sql
# select * from t_group_post where theme_id = 13 and (id < xxx or (id = xxx));
