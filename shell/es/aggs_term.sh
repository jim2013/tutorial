#!/bin/bash

curl -XGET 'localhost:9200/weather/_search?pretty' -d '
{
	"query":{
		"term":{
			"date": "2017-11-24"
		}
	}

	"aggs":{
		"goodsTypeId":{
			"term":{ "field":"goods_type_id" }
		}
	}
}
'
