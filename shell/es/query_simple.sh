#!/bin/bash

#ip='localhost'
ip='192.168.5.203'
curl -H'Content-Type:application/json' -XGET ${ip}':9200/book_v2/_search?pretty' -d '
{
  "from": 0,
  "size": 20,
  "timeout": "1000s",
  "query": {
	  "constant_score": {
			"filter":{
				"bool":{
					"should":[
						{"bool":{
							"must":[
								{"term":{"censor_flag":1}},
								{"term":{"book_name_term":"国家"}}
							]
							}
						}
						,
						{"bool":{
							"must":[
								{"term":{"censor_flag":2}},
								{"term":{"book_name":"国"}}
							]
							}
						}
					]
				}
			}
		}	
	}
}
'
