#!/bin/bash

curl -XGET 'localhost:9200/weather/_search?pretty' -d '
{
	"query":{
		"range":{
			"date":{
				"gte":"2017-11-24"
			}
		}
	}
}
'
