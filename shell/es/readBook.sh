#!/bin/bash

#ip='localhost'
ip='192.168.5.203'
curl -XGET ${ip}':9200/read_book_v2/_search?pretty' -d '
{
    "_source": true,
    "from": 0,
    "size": 40,
    "sort": [
        {
            "buy_count": {
                "order": "desc"
            }
        }
    ],
    "timeout": 60000
}
'
