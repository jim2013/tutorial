#!/bin/bash

curl -X GET "172.16.6.5:9200/book_v2/b/97186675/_explain?pretty" -H 'Content-Type: application/json' -d'
{
      "query" : {
		  "boosting":{
			"positive":{
				"match":{"book_name": "测试"}
			},
			"negative":{
			  "match":{"book_name": "a"}
			},
			"negative_boost": 0.5
		  }
      }
}
'

