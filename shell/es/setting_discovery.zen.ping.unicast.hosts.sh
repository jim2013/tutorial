#!/bin/bash

ip='192.168.1.96' 
curl -H'Content-Type:application/json' -XPUT ${ip}':9201/_cluster/settings?pretty' -d '
{ 
	"transient":{
   		"discovery.zen.ping.unicast.hosts" : ["lrts96","lrts98", "lrts100", "lrts102", "lrts184"]
	}
}
'
