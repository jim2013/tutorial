#!/bin/bash

ip='192.168.1.96' 
curl -H'Content-Type:application/json' -XPUT ${ip}':9200/_cluster/settings?pretty' -d '
{ 
	"transient":{
   		"discovery.zen.minimum_master_nodes" : 3
	}
}
'
