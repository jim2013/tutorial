#!/bin/bash

curl -H'Content-Type:application/json' -XGET '172.16.6.5:9200/book_v2/_search?pretty' -d '
{
	"query":{
	  "bool":{
	    "must":[
			{"match_phrase":{"book_name":"片花"}},
			{"term":{"pay_type" : 1}}
		]
	  }
	},
	"from":0,
	"size": 3,
	"stored_fields":["book_id", "book_name"]
}
'
