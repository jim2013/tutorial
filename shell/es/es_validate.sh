#!/bin/bash

ip='localhost'
#ip='192.168.5.203'
curl -XGET ${ip}':9200/weather/_validate/query?pretty&explain' -d '
{
	"query":{
		"bool":{
			"must":[
				{"term":{"country":"CN"}},
				{"term":{"date":"2017-03-14"}}
			]
		}
	}
}
'
