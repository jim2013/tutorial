package song.concurrency;

public class WithoutTermination {

	public static void main(String[] args) throws Exception{
		Thread thread1 = new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					while (true) {
						System.out.println("hello from unTerimination thread!");
							Thread.sleep(1000);
					}
				} catch (InterruptedException e){

				}
			}
		});	
		thread1.start();

		Thread thread2 = new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					while (true) {
						System.out.println("hello from terimination thread!");
						Thread.sleep(1000);
					}
				} catch (InterruptedException e){

				}
			}
		});	
		thread2.start();

		Thread.sleep(3000);
		thread2.interrupt();
	}

}
