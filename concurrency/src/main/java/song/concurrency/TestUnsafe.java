package song.concurrency;

import sun.misc.Unsafe;
import java.lang.reflect.Constructor;
public class TestUnsafe{
	public static void main(String[] args) throws Exception{
		Constructor<Unsafe> unsafeConstructor = Unsafe.class.getDeclaredConstructor();
		unsafeConstructor.setAccessible(true);
		Unsafe unsafe = unsafeConstructor.newInstance();
	}
}
