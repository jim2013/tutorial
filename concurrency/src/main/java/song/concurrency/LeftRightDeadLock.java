package song.concurrency;

/**
 * Lock-ordering DeadLock
 * TO trigger a thread dump, you can send the JVM process a SIGQUI(kill -3) on unix platforms.
 * the thread dump will show the dead lock information
 */
public class LeftRightDeadLock{
	private static final Object left = new Object();
	private static final Object right = new Object();
	
	public static void leftRight() throws InterruptedException{
		synchronized(left){
			System.out.println("1.1 get left lock.");
			synchronized(right){
				System.out.println("1.2 get right lock");
				Thread.sleep(5);
			}
		}
	}

	public static void rightLeft() throws InterruptedException{
		synchronized(right){
			System.out.println("2.1 get right lock.");
			synchronized(left){
				System.out.println("2.2 get left lock.");
				Thread.sleep(5);
			}
		}
	}

	public static void main(String[] args) {
		Thread thread1 = new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					int c = 1;
					while (true) {
						leftRight();	
						System.out.println("leftRight count:" + c++);
					}
				} catch (InterruptedException e){
					
				}
			}
		});	
		thread1.start();

		Thread thread2 = new Thread(new Runnable(){
			@Override
			public void run(){
				try {
					int c = 1;
					while (true) {
						rightLeft();
						System.out.println("rightLeft count:" + c++);
					}
				} catch (InterruptedException e){

				}
			}
		});	
		thread2.start();
	}

}
