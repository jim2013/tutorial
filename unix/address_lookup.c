#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>

/**
 * ## what is address lookup for?
 * Idealy, an application simply pass socket address around as sockaddr 
 * structures and doesn't rely on any protocl-specific features, then
 * the application will work with many different protocols that provide
 * the same type of service
 *
 *
 *
 */

char *str_family(int ai_family)
{
	switch (ai_family)
	{
		case AF_INET:
			return "inet";
		case AF_INET6:
			return "inet6";
		case AF_UNIX:
			return "unix";
		case AF_UNSPEC:
			return "unspecified";
		default:
			return "unknown";
	}
}

char *str_socktype(int socktype)
{
	switch (socktype)
	{
		case SOCK_STREAM:
			return "stream";
		case SOCK_DGRAM:
			return "datagram";
		case SOCK_SEQPACKET:
			return "seqpacket";
		case SOCK_RAW:
			return "raw";
		default:
			return "unknown";
	}
}

char *str_protocol(int protocol)
{
	switch (protocol)
	{
		case 0:
			return "default";
		case IPPROTO_TCP:
			return "tcp";
		case IPPROTO_UDP:
			return "udp";
		case IPPROTO_RAW:
			return "raw";
		default:
			return "unknown";
	}
}

int str_flags(int flag, char *str)
{
	if (flag == 0){
		str = "-";
		return 0;
	}
	int pos = 0;
	if (flag & AI_PASSIVE)
	{
		strcpy(str+pos, "+passive");
		pos += 8;
	}
	if (flag & AI_CANONNAME)
	{
		strcpy(str+pos, "+canon");
		pos += 6;
	}
	if (flag & AI_NUMERICHOST)
	{
		strcpy(str+pos, "+numhost");
		pos += 8;
	}
	if (flag & AI_NUMERICSERV)
	{
		strcpy(str+pos, "+numserv");
		pos += 8;
	}
	if (flag & AI_V4MAPPED)
	{
		strcpy(str+pos, "+v4mapped");
		pos += 9;
	}
	if (flag & AI_ALL)
	{
		strcpy(str+pos, "+all");
		pos += 4;
	}
	return 0;
}

/**
 * get the hosts known by a computer
 */
void t_hostent()
{
	struct hostent *p = NULL;
	/**
	 * void sethostnet(int stayopen)
	 * open the file or rewind it if it is already open.
	 * When stayopen argument is set to a non zero value, the file remains open
	 * after calling gethostent()
	 */
	sethostent(1);
	/**
	 * will read information from /etc/hosts line by line
	 */
	while ((p = gethostent()) != NULL)
	{
		printf("h_name:%s h_addrtyp:%d h_length:%d\n", p->h_name, p->h_addrtype, p->h_length);
	}
	/**
	 * close file
	 */
	endhostent();
}

/**
 * similar to t_hostent
 */
void t_netent()
{
	struct netent *p = NULL;
	setnetent(1);
	/**
	 * getnetent: get networks entries
	 * will read information from /etc/networks line by line
	 * for more information about /etc/networks, see man networks(5)
	 */
	while((p = getnetent()) != NULL)
	{
		printf("n_name:%s n_addresstype:%d network number:%d\n", p->n_name, p->n_addrtype, ntohl(p->n_net));
	}
	endnetent();
}

void t_protoent()
{
	struct protoent *p = NULL;
	setprotoent(1);
	/**
	 * read information from /etc/protocols
	 */
	while((p = getprotoent()) != NULL)
	{
		printf("p_name:%s, p_proto:%d\n", p->p_name, p->p_proto);
	}
	endprotoent();
}

void t_servent()
{
	struct servent *p = NULL;
	setservent(1);
	/**
	 * read information from /etc/services
	 */
	while((p = getservent()) != NULL)
	{
		printf("s_name:%s, s_port:%d, s_proto:%s\n", p->s_name, p->s_port, p->s_proto);
	}
	endservent();
}

void t_getaddrinfo()
{
	/**
	 * We can apply an optional hint to select address that meet certain criteria.
	 * The hint uses only the ai_family, ai_flags, ai_protocol, and ai_socketype
	 * fields
	 */
	struct addrinfo hint;
	/**
	 * AI_CANONAME:Request a canonical name(as opposed to an alias)
	 */
	hint.ai_flags = AI_CANONNAME;
	hint.ai_family = 0;
	hint.ai_socktype = 0;
	hint.ai_protocol = 0;
	hint.ai_addrlen = 0;
	hint.ai_canonname = NULL;
	hint.ai_addr = NULL;
	hint.ai_next = NULL;

	int err = 0;
	char service[6] = "80";
	struct addrinfo *ailist;
	err = getaddrinfo("127.0.0.1", service, &hint, &ailist);
	if (err != 0){
		printf("getaddrinfo error: %s\n", gai_strerror(err));
	} else {
		struct addrinfo *pai;
		for ( pai = ailist; pai != NULL; pai = pai-> ai_next)
		{
			char strFlag[48];
			str_flags(pai->ai_flags, strFlag);
			printf("ai_flags:%s, ai_family:%s, ai_socktype:%s, ai_protocol:%s"
					, strFlag
					, str_family(pai->ai_family)
					, str_socktype(pai->ai_socktype)
					, str_protocol(pai->ai_protocol));
			printf("\n");
		}
		freeaddrinfo(ailist);
	}

}

int main(int argc, char *argv[])
{
	printf("------hostent example------\n");
	t_hostent();
	printf("------netent example------\n");
	t_netent();
	printf("------protoent example------\n");
	t_protoent();
	printf("------servent example------\n");
	t_servent();
	printf("------getaddrinfo example-----\n");
	t_getaddrinfo();
}
