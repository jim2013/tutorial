#include <stdio.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <fcntl.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>   //strcpy
#include <stddef.h>	//offsetof
#include <unistd.h>

/*
# what is ancillary data?
ancillay data is also called control messages that are not part of the socket
payload.
This control information may include the interface the packet was received on, 
various rarely used header fields, an extended error description, a set of file
descriptors, or UNIX credentials.

# ancillary data structure

//c stands for control
struct cmsghdr {
	// Data byte count, including header (type is socklen_t in POSIX) 
    size_t cmsg_len;    
	// Originating protocol 
   int    cmsg_level;  
	// Protocol-specific type 
   int    cmsg_type;   
   // followed by actual control msg data
   unsigned char cmsg_data[]; 
};

# sendmsg

struct msghdr {

}


*/

int NUM_FD = 1;

void
sendfd(int clientfd, int fd_to_send)
{
    struct msghdr msg = { 0 };
    struct cmsghdr *cmsg;
    int myfds[NUM_FD];  /* Contains the file descriptors to pass */
	myfds[0] = fd_to_send;
    char iobuf[2] = {'f'};
	iobuf[0] = 'e';
    struct iovec io = {
        .iov_base = iobuf,
        .iov_len = sizeof(iobuf)
    };
    union {         /* Ancillary data buffer, wrapped in a union
                       in order to ensure it is suitably aligned */
        char buf[CMSG_SPACE(sizeof(myfds))];
        struct cmsghdr align;
    } u;

    msg.msg_iov = &io;
    msg.msg_iovlen = 1;
    msg.msg_control = u.buf;
    msg.msg_controllen = sizeof(u.buf);
    cmsg = CMSG_FIRSTHDR(&msg);
    cmsg->cmsg_level = SOL_SOCKET;
    cmsg->cmsg_type = SCM_RIGHTS;
    cmsg->cmsg_len = CMSG_LEN(sizeof(int) * NUM_FD);
    memcpy(CMSG_DATA(cmsg), myfds, NUM_FD * sizeof(int));
	printf("server send msg_controllen is %d\n", msg.msg_controllen);
	int send_bytes = sendmsg(clientfd, &msg, 0);
	if (send_bytes == -1)
	{
		perror("sendmsg");
	} else {
		printf("send %d bytes\n", send_bytes);
	}
	sleep(3);
}

int
main(int argc, char *argv[])
{
	int fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
	{
		perror("fail to create socket");
		return 1;
	}

	struct sockaddr_un addr;
	memset(&addr, 0, sizeof(struct sockaddr));
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, "my_unix_domain");
	int addrLen = offsetof(struct sockaddr_un, sun_path) 
		+ strlen(addr.sun_path) + 1;
	if (bind(fd, (struct sockaddr*)&addr, addrLen) == -1)
	{
		perror("fail to bind");
		return 1;
	}
	if (listen(fd, 10) == -1)
	{
		perror("listen");
		return 1;
	}

	struct sockaddr_un clientAddr;
	memset(&clientAddr, 0, sizeof(struct sockaddr_un));
	socklen_t clientLen = sizeof(clientAddr);
	int clientFD = accept(fd, (struct sockaddr *)&clientAddr, &clientLen);
	if (clientFD == -1)
	{
		perror("accept");
		return 1;
	} 
	char buf[256] = {0};
	int n = read(clientFD, buf, 256);
	if (n < 0)
	{
		perror("read");
		return 1;
	}
	printf("receive msg:%s\n", buf);
	int fd_to_send = open("a.txt", O_RDONLY);
	if (fd_to_send == -1 )
	{
		perror("open");
		return 1;
	}

	sendfd(clientFD, fd_to_send);
	printf("will close fd");
	close(fd);
	close(clientFD);
	unlink("my_unix_domain");
}

