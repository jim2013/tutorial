#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

void 
print_fd_flags(int fd)
{
	int fd_flags = fcntl(fd, F_GETFD, 0);
	if (fd_flags == -1)
	{
		printf("fail to get file descriptor flags\n");
		return;
	}
	if (fd_flags & FD_CLOEXEC == FD_CLOEXEC)
	{
		printf("FD_CLOEXE flag set");
	} else {
		printf("FD_CLOEXE flag is not set");
	}
}

void
print_file_flags(int fd)
{
	int file_flags = fcntl(fd, F_GETFL, 0);
	if (file_flags == -1)
	{
		printf("fail to get file flags\n");
		return;
	}
	switch (file_flags & O_ACCMODE)
	{
		case O_RDONLY:
			printf("read only");
			break;
		case O_WRONLY:
			printf("write only");
			break;
		case O_RDWR:
			printf("read write");
			break;
		default:
			printf("unknown access mode");
			break;
	}
	if (file_flags & O_APPEND)
	{
		printf(", append");
	}
	if (file_flags & O_NONBLOCK)
	{
		printf(", nonblocking");
	}
	if (file_flags & O_SYNC)
	{
		printf(", synchronous writes");
	}
}

void
print_flags(int fd, const char *fd_name)
{
	printf("%s fd flags:", fd_name);
	print_fd_flags(fd);
	printf("\n");
	printf("%s file flags:", fd_name);
	print_file_flags(fd);
	printf("\n");
}

void
set_fd(int fd, int flags)
{
	int val = 0;
	if ((val = fcntl(fd, F_GETFD, 0)) == -1)
	{
		printf("fail to get file descriptor flags");
		return;
	}

	val |= flags;
	if (fcntl(fd, F_SETFD, val) == -1)
	{
		printf("fail to set file descriptor flags");
		return;
	}
}

void
set_fl(int fd, int flags)
{
	int val = 0;
	if ((val = fcntl(fd, F_GETFL, 0)) == -1)
	{
		printf("fail to get file flags");
		return;
	}

	val |= flags;
	if (fcntl(fd, F_SETFL, val) == -1)
	{
		printf("fail to set file flags");
		return;
	}
}

int
main(int argc, char *argv[])
{
	if (argc < 2){
		printf("useage: a.out filenpath\n");
		return -1;
	}

	int fd1 = open(argv[1], O_RDONLY);
	if (fd1 == -1){
		printf("fail to open file %s\n", argv[1]);
		return -1;
	}
	print_flags(fd1, "fd1");

	int fd2 = dup(fd1);
	if (fd2 == -1)
	{
		printf("fail to dup fd1\n");
		return -1;
	}
	print_flags(fd2, "fd2");

	int fd3 = open(argv[1], O_RDWR|O_APPEND);
	if (fd3 == -1)
	{
		printf("fail to open file %s\n", argv[1]);
		return -1;
	}
	print_flags(fd3, "fd3");

	printf("change fd1 file flag , file descriptor flag\n");
	set_fd(fd1, O_CLOEXEC);
	set_fl(fd1, O_NONBLOCK|O_SYNC);

	print_flags(fd1, "fd1");
	print_flags(fd2, "fd2");
	print_flags(fd3, "fd3");

	return 0;
}
