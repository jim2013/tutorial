#include<stdio.h>
#include<errno.h>
#include<strings.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>

/**
 * # what it is
 * It's a simple udp server for demostration, it just prints whatever it receive
 * 
 * # how to test it
 * You can use cli tools nc as a udp client to play with the udp server, 
 * nc command: nc -u -4 localhost:9002 hello
 *
 * # security concern
 * In practice, you probably shouldn't write such a server, due to a risk of 
 * becoming a DoS reflection vector. UDP services should always respond with a
 * strictly smaller amount of data than was sent in the initial packet
 *
 */
int
main(int argc, char *argv[])
{
	/* 1. open socket */
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0)
	{
		perror("fail to open socket: ");
		return -1;
	}

	/* 2. bind socket to ip and port */
    struct sockaddr_in servAddr;
    bzero((char *) &servAddr, sizeof(servAddr));
    servAddr.sin_family = AF_INET;
    servAddr.sin_addr.s_addr = INADDR_ANY;
    servAddr.sin_port = htons(9002);	
    if (bind(fd, (struct sockaddr *) &servAddr,
             sizeof(servAddr)) < 0) 
	{
	    perror("fail to bind: ");
		return -1;
	} else {
	    printf("server start, listen on localhost:9002\n");
	}

	/* 3. receive from client */
	struct sockaddr_in clientAddr;
	int addrLen = sizeof(clientAddr);
	bzero((char *)&clientAddr, addrLen);
	char buf[32] = {'\0'};
	recvfrom(fd, buf, 32, 0, (struct sockaddr*)&clientAddr, &addrLen);
	printf("receive: %s\n", buf);

	/* 4. close socket */
	close(fd);

}
