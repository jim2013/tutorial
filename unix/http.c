/**
 * A simple http client for leaning http
 * */

#include <arpa/inet.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <unistd.h>

#include <string.h>

int main(int argc, char *argv[])
{
	/* query ipv4 and tcp communication address*/
	struct addrinfo hint, *res0;
	memset(&hint, 0, sizeof(hint));
	hint.ai_family = AF_INET;
	hint.ai_socktype = SOCK_STREAM;
	char *host = "songziyu.cc";
	int ret;
	if ((ret = getaddrinfo(host, NULL, &hint, &res0)) != 0)
	{
		printf("fail to look ip of %s, err:%s\n", host, gai_strerror(ret));
		return 1;
	}
	
	struct sockaddr_in *addr = res0->ai_addr;
	addr->sin_port = htons(80);

	int fd = socket(res0->ai_family, res0->ai_socktype, res0->ai_protocol);
	if (fd == -1) goto err;
	if (connect(fd, res0->ai_addr, res0->ai_addrlen) == -1) goto err;
	char *msg = "GET /rss/cz_binance.xml HTTP/1.1\r\nHost: songziyu.cc\r\nUser-Agent: mhttp\r\n\r\n";
	if (write(fd, msg, strlen(msg)) != strlen(msg)) goto err;

	printf("Request:\n%s Response:\n", msg);
	char buf[1024] = {'\0'};
	int rsize = 0;
	while((rsize = read(fd, buf, 1023)) > 0)
	{
		printf("%s", buf);
		memset(buf, '\0', 1024);
	}
	if (rsize == -1)  goto err;

	freeaddrinfo(res0);
	return 0;

err:
	perror(NULL);
	freeaddrinfo(res0);
	return -1;
}
