#include <stdio.h>
#include <sys/stat.h>	//stat
#include <fcntl.h>	//open
#include <unistd.h> //close
#include <string.h> //strcat

void
topen(char *dir)
{
	struct stat sb;
	if (stat(dir, &sb) == -1)
	{
		perror(dir);
	} else {
		printf("will create file, press any key to continue...\n");
		getchar();
		getchar();
		int fd = -1;
		char filename[255];
		strcpy(filename, dir);
		strcat(filename, "/a.txt");
		if ((fd = open(filename, O_CREAT|O_RDWR, S_IRUSR | S_IWUSR | IRGRP | IWGRP)) == -1)
		{
			perror(filename);
		} else {
			printf("create file successfully~\n");
			close(fd);
		}
	}
}

void
topenCWD()
{
	int dir = open("/home/song/workspace/tutorial/unix/bin", O_RDONLY);
	if (dir == -1)
	{
		perror("");
		return;
	}
	printf("will create file, press any key to continue...\n");
	getchar();
	getchar();
	char *filename = "a.txt";
	int fd = open(filename, O_CREAT|O_RDWR, S_IRUSR | S_IWUSR | IRGRP | IWGRP);
	if (fd == -1)
	{
		perror(filename);
	} else {
		printf("create file successfully\n");
		close(fd);
	}
}

void topenAt()
{
	
	int dir = open("/home/song/workspace/tutorial/unix/bin", O_RDONLY);
	if (dir == -1)
	{
		perror("");
		return;
	}
	printf("will create file, press any key to continue...\n");
	getchar();
	getchar();
	char *filename = "a.txt";
	int fd = openat(dir, filename, O_CREAT, S_IRUSR | S_IWUSR | IRGRP | IWGRP);
	if (fd == -1)
	{
		perror(filename);
	} else {
		printf("create file successfully\n");
		close(fd);
	}
}

int
main(int argc, char *argv[])
{
//	topen("/tmp/data");
	topenAt();
//	topenCWD();
}
