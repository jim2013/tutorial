#include <stdio.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	/**
	 * The stdout stream is line buffered by default, so will not display
	 * what's in the buffer until it reaches a newline. We have a few option
	 * to print immediately:
	 * 1. Flush stdout whenever you need it by calling fflush(stdout)
	 * 2. print to stderr instead using fprintf(stderr is unbufferd by default)
	 * 3. disable buffering on stdout by setbuf
	 *
	 * !! Be care about: some operation will fulsh stdout, it will make you feel
	 * that stdout buffer does not existed.for example read from stdin will
	 * fulsh stdout buffer.
	 */
	setbuf(stdout, NULL);
	printf("you can not see me immediately if you don't turn off stdout buffer.");
	sleep(3);
}
