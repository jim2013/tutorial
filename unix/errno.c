#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>
#include <string.h>  //strerror()

/**
 * When an error occurs in one of the UNIX System functions, a negative value 
 * is often returned, and the integer errno is usually set to a value that 
 * tells why
 */
int
main(int argc, char* argv[])
{
	struct stat sb;
	if (stat("./not_exist.txt", &sb) == -1)
	{
		printf("errorno is: %d\n", errno);
		printf("error msg produced by strerror(): %s\n", strerror(errno));
		perror("error msg produced by perror");
	}
	if (stat("./errno.c", &sb) == 0)
	{
		//errno is never cleared by a routine if an error does not occur
		perror("last error msg is");
	} 
}
