#include<stdio.h>
#include<unistd.h>

/**
 * ## what is pipe for?
 * IPC:Interprocess Communication
 * Pipes are the oldest form of UNIX System IPC and provides by all UNIX System
 * Pipes have two limitations:
 * 1. Historically, they have been half duplex(i.e data flow only in one 
 * direction)
 * 2. Pipes can be used only between processes that have a common ancestor
 *
 * ## how to create a pipe
 * A pipe is created by calling the pipe function
 * int pipe(int fd[2]); 
 * Two file descriptor is returned through fd argument:
 * fd[0] is open for reading
 * fd[1] is open for writing
 * 
 * fd[1](user process) ---> pipe(kernel) ----> fd[0](user process)
 *
 * ## data from parent process --> child process
 * parent process close the read end of the pipe(fd[0])
 * child process close the write end of the pipe(fd[1])
 *
 * ## data from child process --> parent process
 * parent close fd[1]
 * child close fd[0]
 *
 * ## When one end of a pipe is closed, two rules apply
 * 1. If we read from a pipe whose write end has been closed, read return 0 to
 * indicate an end of file after all the data have been read.
 * 2. If we write to a pipe whose read end has been closed, the signal SIGPIPE
 * is generated. If we either ignore the signal or catch it and from the signal
 * handler, write returns -1 with errno set to EPIPE
 *
 * ## PIP_BUF
 * When we're writing to a pipe(or FIFO), the constant PIP_BUF specifies the
 * kernel's pip buffer size. A write of PIP_BUF bytes or less will not be
 * interleaved with the writes from other process to the same pipe(or FIFO).
 *
 */
int main(int argc, char *argv[])
{
	int n;
	int fd[2];
	pid_t pid;
	char line[100];
	
	if (pipe(fd) < 0){
		printf("fail to pipe.\n");
		return 1;
	}
	if ((pid = fork()) < 0){
		printf("fail to fork.\n");
		return 1;
	}

	if (pid > 0){	/* parent*/
		close(fd[0]);
		write(fd[1], "hello world\n", 12);
	} else {	/* child */
		close(fd[1]);
		n = read(fd[0], line, 100);
		printf("%s", line);
	}
}
