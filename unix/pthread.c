#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<sys/types.h>
#include<unistd.h>

/**
 * ## process vs thread
 * 1. Different processes are independent.Mutiplple processes have to use 
 * complex mechanisms provided by operating system to share memeory and file
 * descriptor(IPC)
 * 2. All threads within a signal process have access to the same process 
 * components, such as file descriptors and memory.
 * 3. Anytime you try to share a signal resource among multiple threads, you 
 * have to deal with consistency
 *
 * ## Thread ID(indetification)
 * 1. process id is unique in the system, while thread id has significance 
 * only within the context of the process to which it belongs.
 * 2. Process id is a non-negative integer, while thread id is different.
 * thread id is represented by the pthread_t data type. Linux 3.2.0 uses an 
 * unsigned long integer for the pthread_t data type. FreeBSD 8.0 and Mac OS X
 * 10.6.8 use a pointer to the pthread structure for the ptread_t data type
 *
 * #include<ptread.>
 * // compare two ptread id
 * int ptread_equal(pthread_t tid1, pthread_t tid2)
 * // obtain its own thread id
 * pthread_t pthread_self(void)
 *
 * ## how to create a thread
 *
 * int pthread_create(pthread_t *restrict tidp, 
 *					const pthread_attr_t *restrict attr, 
 *					void *(*start_rtn)(void *), void *restrict arg);
 * ## thread termination
 * If any thread within a process calls exit, _Exit, or _exit, the entire 
 * process terminates.
 * A single thread can exit in three ways, without terminating the entire 
 * process:
 * 1. Return from the start function
 * 2. The thread can be canceled by another thread in the same process
 * 3. The thread can call pthread_exit
 *
 * ## condition variables
 * Condition variables are anthor synchronization mechanism avaliable to 
 * threads.
 * The condition itself is protected by a mutex.
 *
 * common work flow
 *
 * threadA --> get mutex --> wait for some state of the condition variable
 *         --> do some tasks --> release the mutex
 *
 * threadB --> get mutex --> do other tasks --> signal the condition
 *         --> release mutex
 * 
 * ## compile this file
 * gcc -lpthread pthread.c
 */

void * thr_fn(void * arg)
{
	printf("new thread: pid %lu tid %lu (0x%lx)\n", (unsigned long)getpid(),
		   	(unsigned long)pthread_self(),(unsigned long)pthread_self());
}

void create()
{
	pid_t pid;
	pthread_t tid;
	int err;
	/**
	 * @parameter tidp
	 * @parameter attr: customize new thread attributes. when setting this to
	 * NULL, create new thread with default attributes.
	 * @parameter thr_fn: new thread starts running at the address of the
	 * thr_fn function
	 * @parameter arg: the argument for thr_fn function 
	 * 
	 * @return 0:success, others fail
	 */
	err = pthread_create(&tid, NULL, thr_fn, NULL);
	if (err != 0){
		printf("fail to create thread\n");
		exit(0);
	}

	printf("new thread: pid %lu tid %lu (0x%lx)\n", (unsigned long)getpid(),
		   	(unsigned long)pthread_self(),(unsigned long)pthread_self());
	sleep(1);
	exit(0);
}

int main(int argc, char* argv[]){
	int o = 1;
	if (o == 1){
		create();
	}
}
