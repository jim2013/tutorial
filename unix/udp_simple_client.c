#include<arpa/inet.h>
#include<errno.h>
#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>

/**
 * # reference
 * https://blog.cloudflare.com/everything-you-ever-wanted-to-know-about-udp-sockets-but-were-afraid-to-ask-part-1/
 * 
 */

int
connectedClient()
{
	/* 1.1 open socket */
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0)
	{
		perror("fail to open socket: ");
		return -1;
	}

	/* 1.2 bind to an address (optional) */
	struct sockaddr_in clientAddr;
	bzero((char *)&clientAddr, sizeof(clientAddr));
	clientAddr.sin_family = AF_INET;
	struct in_addr client_in_addr;
	/* we can only bind a socket to an address of a local interface */
	int atonRet =  inet_aton("127.0.0.1", &client_in_addr);
	if (atonRet == 0)
	{
		printf("fail to convert client in addr\n");
		return -1;
	}
	clientAddr.sin_addr = client_in_addr;
	if (bind(fd, (struct sockaddr *)&clientAddr, sizeof(clientAddr)) < 0)
	{
		perror("fail to bind client address: ");
		return -1;
	}

	/* 2. initialize server address and connect to server */
	struct sockaddr_in servAddr;
	int addrLen = sizeof(servAddr);
	bzero((char *)&servAddr, addrLen);
	servAddr.sin_family = AF_INET;
	struct in_addr sin_addr;
	atonRet = inet_aton("127.0.0.1", &sin_addr);
	if (atonRet == 0)
	{
		printf("fail to convert server address.\n");
		return -1;
	}
	servAddr.sin_addr = sin_addr;
    servAddr.sin_port = htons(9002);	
	if (connect(fd, (struct sockaddr*)&servAddr, addrLen) == -1)
	{
		perror("fail to connect to server: ");
		return -1;
	}

	/* 3.1 send hello */
	char buf[64] = "hello from connected udp client\n";
	send(fd, buf, strlen(buf), 0);

	/* 3.2 besides bind(), another way to set source ip for a UDP with the help
	 * of IP_PKTINFO */
	struct msghdr msg;
	memset(&msg, 0, sizeof(msg));
	struct iovec msg_data;
	char * str_hi = "hello from connected udp client by sendmsg\n";
	msg_data.iov_base = str_hi;
	msg_data.iov_len = strlen(str_hi);
	msg.msg_iovlen = 1;
	msg.msg_iov = &msg_data;
	/* set source ip address */
	int msg_controllen = CMSG_SPACE(sizeof(struct in_pktinfo));
	msg.msg_control = malloc(msg_controllen);
	struct cmsghdr *cmsg;
	cmsg = CMSG_FIRSTHDR(msg);
	cmsg->cmsg_level = IPPROTO_IP;
	cmsg->cmsg_type = IP_PKTINFO;
	cmsg->cmsg_len = CMSG_LEN(sizeof(struct in_pktinfo));
	struct in_pktinfo *pktinfo = (struct in_pktinfo*) CMSG_DATA(cmsg);
	pktinfo->ipi_ifindex = src_interface_index;
	pktinfo->ipi_spec_dst = src_addr;

	sendmsg(fd, &msg, 0);

	/*4. close */
	close(fd);
}

/**
 * In unconnected socket, the programmer should verify the source IP of the 
 * packet.Otherwise, the program can get confused by some random inbound
 * internet junk -- like port scanning
 */
int
unconnectedClient()
{
	/* 1. open socket */
	int fd = socket(AF_INET, SOCK_DGRAM, 0);
	if (fd < 0)
	{
		perror("fail to open socket: ");
		return -1;
	}

	/* 2. initialize server address */
	struct sockaddr_in servAddr;
	int addrLen = sizeof(servAddr);
	bzero((char *)&servAddr, addrLen);
	servAddr.sin_family = AF_INET;
	struct in_addr sin_addr;
	int atonRet = inet_aton("127.0.0.1", &sin_addr);
	if (atonRet == 0)
	{
		printf("fail to convert server address.\n");
		return -1;
	}
	servAddr.sin_addr = sin_addr;
    servAddr.sin_port = htons(9002);	

	/* 3.1 send msg */
	char buf[64] = "hello from unconnected udp client\n";
	sendto(fd, buf, strlen(buf), 0, (struct sockaddr*)&servAddr, addrLen);

	/* 3.2 send msg to anthor server with the same fd */
	atonRet = inet_aton("149.28.228.191", &sin_addr);
	if (atonRet == 0)
	{
		printf("fail to convert anthor server address.\n");
		return -1;
	}
	servAddr.sin_addr = sin_addr;
	sendto(fd, buf, strlen(buf), 0, (struct sockaddr*)&servAddr, addrLen);

	/*4. close */
	close(fd);
}

int
main(int argc, char *argv[])
{
	connectedClient();
//	unconnectedClient();
}
