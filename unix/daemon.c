#include <stdio.h>
#include <errno.h>
#include <syslog.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/resource.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>

void
daemonize(const char *cmd)
{
	int fd0, fd1, fd2;
	pid_t pid;
	struct rlimit rl;
	struct sigaction sa;

	/**
	 * clear file creation mask
	 */
	umask(0);

	/**
	 * Get maximum number of file descriptors
	 */
	if (getrlimit(RLIMIT_NOFILE, &rl) < 0)
	{
		perror("can't get file limit");
		exit(1);
	}


	/**
	 * Became a session leader to lose controlling TTY
	 */
	if ((pid = fork()) < 0)
	{
		perror("fail to fork");
		exit(0);
	} else if (pid != 0) {
		/*parent*/
		exit(0);
	}
	setsid();

	/*
	* Initialize the log file.
	*/
	openlog(cmd, LOG_CONS, LOG_DAEMON);

	/**
	 * Change the current working directory to the root so
	 * we won’t prevent file systems from being unmounted.
	 */
	if (chdir("/") < 0)
	{
		syslog(LOG_ERR, "fail to change dir");
		exit(1);
	}

	/**
	 * Close all open file descriptors
	 */
	if (rl.rlim_max == RLIM_INFINITY)
	{
		rl.rlim_max = 1024;
	}
	for (int i = 0; i < rl.rlim_max; i++)
		close(i);

	/**
	 * Attach file descriptor 0, 1, 2 to /dev/null
	 */
	fd0 = open("/dev/null", O_RDWR);
	fd1 = dup(fd0);
	fd2 = dup(fd0);

	if (fd0 != 0 || fd1 != 1 || fd2 != 2) {
		syslog(LOG_ERR, "unexpected file descriptors %d %d %d",
		fd0, fd1, fd2);
		exit(1);
	}

	/**
	 * write pid to log
	 */
	syslog(LOG_INFO, "pid is %d", getpid());
}

int
main(int argc, char *argv[])
{
	daemonize(argv[0]);
	sleep(60);
	syslog(LOG_INFO, "exit");
}
