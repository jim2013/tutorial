#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <errno.h>
#include <time.h>

void print_current_time();
/**
 * ## refs
 * [TCP_NODELAY and Small Buffer Writes](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_MRG/1.2/html/Realtime_Tuning_Guide/sect-Realtime_Tuning_Guide-Application_Tuning_and_Deployment-TCP_NODELAY_and_Small_Buffer_Writes.html)
 *
 * ## what is `tcp_nodelay` option used for?
 * By Default TCP(Transmission Control Protocol) uses Nagle's algorithm to 
 * collect small outgoing packets to send all at once.
 * we can enable tcp_nodelay to improve network latency
 * 
 * ## how
 * int one = 1;
 * setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &one, sizeof(int));
 * 
 * ## detail
 * MSS:maximum segment size
 * if ther is new data to send
 *   if the window size >= MSS and available data is >= MSS
 *     send complete segment now
 *   else
 *     if there is unconfirmed data still in pipe
 *       enqueue data in the buffer until an acknowledge is received
 *     else
 *       send data immediately
 *     end if
 *   end if
 * end if
 *
 * ## TCP delayed acknowledgements(delay ACK)
 * Nagle's algorithm works badly with TCP delayed acknowledgements.
 * write-write-read pattern experience a constant delay of up to 500 milliseconds.
 * why??
 * disable delay ACK:TCP_QUICKACK on linux
 *
 * ## 
 */
int main(int argc, char *argv[])
{
	int lfd, cfd, portno;	
	socklen_t cli_len;
	char buffer[256];
	struct sockaddr_in serv_addr, cli_addr;
	if (argc < 2) {
		printf("ERROR, no port provided\n");
		exit(1);
	}	

	//open tcp socket
	lfd = socket(AF_INET, SOCK_STREAM, 0);
	if (lfd < 0){
		printf("ERROR opening socket");
		exit(1);	
	}
	/**
	 *
	 */
	int reuse = 1;
	if (setsockopt(lfd, SOL_SOCKET, SO_REUSEADDR, &reuse, sizeof(int)) < 0)
	{
		printf("fail to set socket reuse option.\n");
		exit(1);
	}
	bzero((char *)&serv_addr, sizeof(serv_addr));
	portno = atoi(argv[1]);
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	//convert a port number in host byte order to port number in network order
	serv_addr.sin_port = htons(portno);
	//bind tcp socket to address and port
	if (bind(lfd, (struct sockaddr *)&serv_addr, 
				sizeof(serv_addr)) < 0)
	{
		printf("ERROR on binding\n");
		return 1;
	}
	if (listen(lfd, 5) < 0)
	{
		printf("ERROR on listening");
		return 1;
	}
	cli_len = sizeof(cli_addr);
	cfd = accept(lfd, (struct sockaddr *)&cli_addr, &cli_len);
	if (cfd < 0)
	{	
		printf("ERROR on accept");
		return 1;
	}

	/**
	 * set tcp no delay
	 */
/*	int on = 1;
	if (setsockopt(cfd, IPPROTO_TCP, TCP_NODELAY, &on, 
				sizeof(int)) == -1){
		printf("fail to set tcpNodelay:%s", strerror(errno));
		return -1;
	}
*/
	bzero(buffer, 256);
	int n = read(cfd, buffer, 255);
	if (n < 0)
	{
		printf("ERROR reading from socket");
		return 1;
	}
	printf("Here is the message:%s\n", buffer);
	n = write(cfd, "I got your message", 18);
	if (n < 0)
	{
		printf("ERROR writing to socket");
		return 1;
	}
	char input = 'e';
	while(input = getchar()){
		printf("\n");
		if (input == '\n')
			continue;
		printf("will write %c to socket", input);
		print_current_time();
		n = write(cfd, &input, 1);	
		if (n < 0)
		{
			printf("ERROR writing to socket");
			return 1;
		}
	}
	return 0;
}

void print_current_time()
{
	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == 0)
	{
		printf("current time:%lld.%.9ld\n", (long long)ts.tv_sec, ts.tv_nsec);
	} else {
		printf("fail to get current time.\n");
	}
}
