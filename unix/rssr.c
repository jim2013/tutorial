/**
 * # motivation
 * so many rss feeds are blocked in china, we need get the blocked feeds by a 
 * rss relay service
 *
 * # what
 * a. fetch feeds at a remote server periodically
 * b. provide rss feed
 * 
 * # feed storage
 * 1. every rss feed is in a folder, folder name is feed name
 * 2. channel info of feed is stored inside feed folder, file name is 
 * channel.xml
 * 3. every item is stored in a signal file,file name is increasing positive 
 * integer
 * 4. there is a special file record latest 500 items, include item's file name
 * , item's title and pubDate(optional), file name is recent.txt
 *
 * # complie
 * cc -lcurl rssr.c
 * 
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<curl/curl.h>

struct MemoryStruct {
  char *memory;
  size_t size;
};

static size_t
writeMemoryCallback(void *contents, size_t size, size_t nmemb, void *userp)
{
  size_t realsize = size * nmemb;
  struct MemoryStruct *mem = (struct MemoryStruct *)userp;

  char *ptr = realloc(mem->memory, mem->size + realsize + 1);
  if(!ptr) {
    /* out of memory! */
    printf("not enough memory (realloc returned NULL)\n");
    return 0;
  }

  mem->memory = ptr;
  memcpy(&(mem->memory[mem->size]), contents, realsize);
  mem->size += realsize;
  mem->memory[mem->size] = 0;

  return realsize;
}

int
getFeed(char * url, struct MemoryStruct *resp)
{
	int ret = 0;
	CURL *curl = NULL;
	CURLcode res;
	/* init the curl session */
	curl = curl_easy_init();
	if(curl) {
		/* specify url to get, it's required */
		curl_easy_setopt(curl, CURLOPT_URL, url);

		/* send all received data to this function */
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeMemoryCallback);

		/* we pass our resp buffer to the callback function */
		curl_easy_setopt(curl, CURLOPT_WRITEDATA, (void *)resp);

		/* Perform the request, res will get the return code */
		res = curl_easy_perform(curl);
		/* Check for errors */
		if(res != CURLE_OK)
		{
		  fprintf(stderr, "curl_easy_perform() failed: %s\n",
				  curl_easy_strerror(res));
		  ret = 1
		}

		/* always cleanup */
		curl_easy_cleanup(curl);
	} else {
		ret = 1;
	}
	return ret;
}

void parseXml(struct MemoryStruct * resp)
{
	int i = 0;
	int begin = -1;
	int elementBegin = -1;
	char elementName[64] = {'\0'};
	char elementNameSize = 0;
	bool attributeBegin = false;
	while(i < resp->size)
	{
		char ch = *(resp->memory + i);
		++i;

		if (elementStart)
		{	
			elementStart = false;
			if (elementName == "item")
				itemBegin = true;
			/* elementStart_callback(elementName); */
		}
		if (elementEnd)
		{	
			elementEnd = false;
			if (elementName == "item")
				itemBegin = false;
			/* elementEnd_callback(elementName); */
		}

		if (ch == '<')
		{
			if (begin != -1)
			{
				printf("malformed.\n");
				break;
			}
			begin == i;
			elementStart = false;
			elementEnd = false;
			elementNameSize = 0;
			attributeBegin = false;
		} else if (ch == '>') {
			if (begin == -1)
			{
				printf("malformed.\n");
				break;
			}
			if (elementName[0] == '/' || *(resp->memory + i - 1) == '/')
			{
				elementEnd = true;
			} else {
				elementStart = true;
			}
			begin == -1;
		} else {
			if (begin != -1)
			{
				if (ch == ' ') attributeBegin = true;
				if (!attributeBegin) 
					elementName[elementNameSize++] = ch;
			}
		}

		/* content_callback(ch) */

		if (itemBegin) 
		{
			if (begin == -1)
			{
				write(ch, file)
			} else {
				if (elementStart || elementEnd)
					if (elementName != "item") 
					write(<element>, file);
			}
		}
	}
}

int
main(int argc, char *argv[])
{
	char *feedUrl = "https://www.rssboard.org/files/sample-rss-2.xml";
	struct MemoryStruct *resp = malloc(sizeof(struct MemoryStruct));
	resp->memory = malloc(1);
	resp->size = 0;
	if (getFeed(feedUrl, resp) == 0) 
	{
	}
	
	printf("%s", resp->memory);
}
