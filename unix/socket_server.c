//[simple socket tutorial](https://www.linuxhowtos.org/C_C++/socket.htm)
/**
 * ## what is a socket
 * A socket is an abstraction of a communication end point. 
 * 
 * ## what is a socket descriptor
 * Just as they would use file descriptors to access file, application use 
 * socket descriptors to access sockets.
 *
 * The steps involved in establishing a socket on the server side are as follows:
    1. Create a socket with the socket() system call
    2. Bind the socket to an address using the bind() system call. For a server socket on the Internet, an address consists of a port number on the host machine.
    3. Listen for connections with the listen() system call
    4. Accept a connection with the accept() system call. This call typically blocks until a client connects with the server.
    5. Send and receive data
*/
//There are two widely used address domains:unix domain and the internet domain
//There are two widely used socket types, stream sockets, and datagram sockets.

/* A simple server in the internet domain using TCP
   The port number is passed as an argument */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int main(int argc, char *argv[])
{
     int sockfd, newsockfd, portno;
     socklen_t clilen;
     char buffer[256];
     struct sockaddr_in serv_addr, cli_addr;
     int n;
     if (argc < 2) {
         fprintf(stderr,"ERROR, no port provided\n");
         exit(1);
     }
	 /**
	  * #include <sys/socket.h>
	  * int socket(int domain, int type, int protocol);
	  *
	  * domain determines the nature of the commnication, including the address
	  * format
	  * 1. AF_INET: IPv4 Internet Domain
	  * 2. AF_INET6: IPv6 Internet Domain
	  * 3. AF_UNIX: UNIX Domain
	  * 4. AF_UNSPEC: unspecified, is a wildchar that represents "any" domain
	  *
	  * type determines the type of socket, which futher determines the 
	  * communication
	  * characteristics
	  * 1. SOCK_DGRAM: fixed-length, connectionless, unreliable message
	  * 2. SOCK_RAW: datagram interface to IP
	  * 3. SOCK_SEQPACKET: fixed-length, sequenced, reliable, 
	  * connection-oriented message
	  * 4. SOCK_STREAM: sequenced, reliable, bidirectional, connection-oriented 
	  * byte stream
	  */
     sockfd = socket(AF_INET, SOCK_STREAM, 0);
     if (sockfd < 0) 
        error("ERROR opening socket");
     bzero((char *) &serv_addr, sizeof(serv_addr));
     portno = atoi(argv[1]);
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
	//convert a port number in host byte order to a port number in network order
     serv_addr.sin_port = htons(portno);	
     if (bind(sockfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) 
              error("ERROR on binding");
     listen(sockfd,5);
     clilen = sizeof(cli_addr);
     newsockfd = accept(sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
     if (newsockfd < 0) 
          error("ERROR on accept");
     bzero(buffer,256);
     n = read(newsockfd,buffer,255);
     if (n < 0) error("ERROR reading from socket");
     printf("Here is the message: %s\n",buffer);
     n = write(newsockfd,"I got your message",18);
     if (n < 0) error("ERROR writing to socket");
     close(newsockfd);
     close(sockfd);
     return 0; 
}

