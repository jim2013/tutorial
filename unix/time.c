#include <stdio.h>
#define _XOPEN_SOURCE
#include <time.h>
#include <sys/time.h>
#include <errno.h>
#include <string.h>

void test_time()
{
	/**
	 * #include <time.h>
	 * time_t time(time_t * calptr);
	 * @Param calptr: if calptr is not NULL, the time is also stored at the
	 * location pointed to by calptr
	 * @Return value of the time if OK, -1 on error
	 */
	time_t calendar_time = time(NULL);
	if (calendar_time == -1)
	{
		printf("fail to get time!");
	} else {
		printf("get current time by time:%d\n", calendar_time);
	}
}

void test_timestamp_2_str(long timestamp)
{	
	time_t t = timestamp;
	struct tm tm;
	char buf[1024];
	//strftime support %z timezone
	if (gmtime_r(&t, &tm) != NULL && 
			strftime(buf, sizeof(buf), "%Y-%m-%d %H:%M:%S %z", &tm) != 0)
	{
		printf("timestamp:%ld <----> %s\n", t, buf);
	} else {
		printf("gmtime: fail\n");
	}
}

void 
test_str_2_timestamp(char * timestr, char *format)
{
	struct tm tm;
	memset(&tm, 0, sizeof(struct tm));
	//strptime does not support %z timezone
	char *ret = strptime(timestr, format, &tm);
	if (ret != NULL)
	{
		printf("ret is %s\n", ret);
	} else {
		perror("strptime");
		return;
	}
	time_t timestamp =  mktime(&tm);
	if (timestamp == -1)
	{
		perror("mktime");
		return;
	}
	printf("%s <---> %ld\n", timestr, timestamp);
}

/**
 * #include<sys/time.h>
 * int clock_gettime(clockid_t clock_id, struct timespec *tsp);
 *
 * @param clock_id is system clocks. CLOCK_REALTIME:real system time, 
 * CLOCK_MONOTONIC:real system time without no negative jumps
 * CLOCK_PROCESS_CPUTIME_ID: CPU time for calling process
 * CLOCK_THREAD_CPUTIME_ID: CPU time fro calling thread
 * @param tsp timespec structure type defines in terms of seconds and 
 * nanoseconds, it includes at least two fields: 
 * time_t tv_sec;
 * long tv_nsec;
 */
void test_clock_gettime()
{
	struct timespec ts;
	if (clock_getres(CLOCK_REALTIME, &ts) == 0)
	{
		if (ts.tv_sec == 0)
		{
			printf("CLOCK_REALTIME resolution is %ld nanosecond\n", ts.tv_nsec);
		} else {
			printf("CLOCK_REALTIME resolution is %d second", ts.tv_sec);
		}
	} else {
		printf("fail to call clock_getres\n");
	}

	if (clock_gettime(CLOCK_REALTIME, &ts) == 0)
	{
		printf("get current time by clock_gettime:%lld.%.9ld\n", (long long)ts.tv_sec, ts.tv_nsec);		
	} else {
		printf("fail to call clock_gettime()\n");
	}

}

int main(int argc, char *argv[])
{
	test_time();
	test_clock_gettime();
	test_clock_gettime();
	test_timestamp_2_str(1601136538L);
	test_str_2_timestamp("2020-10-01 00:00:00 +0700", "%Y-%m-%d %H:%M:%S %z");
	test_str_2_timestamp("2020-10-01 00:00:00", "%Y-%m-%d %H:%M:%S");
}
