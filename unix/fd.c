#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

/**
 * To the kernel, all open files are referred to by file descriptors. 
 * A file descriptor is non-negative integer.
 * Every Porcess has its own set of file descriptor, which means different process 
 * will have different file descriptors with same value.
 * By convention, UNIX System shells associate file descriptor 0 with the
 * standard input of a process, file descriptor 1 with the standard output, and file 
 * descriptor 2 with the standard error
 * So this program will always print:fd is 3
 */
int main(int argc, char *argv[])
{
	int fd = open("io_epoll.c", O_RDONLY);
	printf("fd is %d\n", fd);
	return 0;
}
//t1
//t2
