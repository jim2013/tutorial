#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>   //strcpy
#include <stddef.h>	//offsetof
#include <unistd.h>

int
main(int argc, char *argv[])
{
	int fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
	{
		perror("fail to create socket");
		return 1;
	}

	struct sockaddr_un addr;
	addr.sun_family = AF_UNIX;
	strcpy(addr.sun_path, "my_unix_domain");
	int addrLen = offsetof(struct sockaddr_un, sun_path) 
		+ strlen(addr.sun_path) + 1;
	if (bind(fd, (struct sockaddr*)&addr, addrLen) == -1)
	{
		perror("fail to bind");
		return 1;
	}
	if (listen(fd, 10) == -1)
	{
		perror("listen");
		return 1;
	}

	struct sockaddr_un clientAddr;
	socklen_t clientLen = sizeof(clientAddr);
	int clientFD = accept(fd, (struct sockaddr *)&clientAddr, &clientLen);
	if (clientFD == -1)
	{
		perror("accept");
		return 1;
	} else {
		printf("accept path:%s\n", clientAddr.sun_path);
	}
	char buf[256] = {0};
	int n = read(clientFD, buf, 256);
	if (n < 0)
	{
		perror("read");
		return 1;
	}
	printf("receive msg:%s\n", buf);
	close(fd);
	close(clientFD);
	unlink("my_unix_domain");
}
