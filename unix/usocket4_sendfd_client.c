#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
	int fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
	{
		perror("socket");
		exit(1);
	}

	struct sockaddr_un servAddr;
	servAddr.sun_family = AF_UNIX;
	strcpy(servAddr.sun_path, "my_unix_domain");
	int addrLen = offsetof(struct sockaddr_un, sun_path) 
		+ strlen(servAddr.sun_path) + 1;
	if (connect(fd, (struct sockaddr *)&servAddr, addrLen) < 0)
	{
		perror("connect");
		exit(1);
	}
	printf("connect to server\n");
	char buf[256] = "hi~";
	int n = write(fd, buf, strlen(buf));
	if (n < 0)
	{
		perror("write");
		exit(1);
	}

	sleep(1);
	/* receive ancillary data*/
	struct msghdr msg = {0};
    char iobuf[1] = {'b'};
	struct iovec iov[1];
	iov[0].iov_base = iobuf;
	iov[0].iov_len = sizeof(iobuf);
	msg.msg_iov = iov;
	msg.msg_iovlen = 1;
	int myfds[1];
    union {         /* Ancillary data buffer, wrapped in a union
                       in order to ensure it is suitably aligned */
        char buf[CMSG_SPACE(sizeof(myfds))];
        struct cmsghdr align;
    } u;
	msg.msg_control = u.buf;
	msg.msg_controllen = sizeof(u.buf);

	ssize_t receive_size = recvmsg(fd, &msg, 0);
	if ( receive_size == -1)
	{
		perror("recvmsg");
	} else {
		printf("client receive size is %d,  receive msg_iovlen is %d, msg_controllen length is %d\n", receive_size, msg.msg_iovlen, 
				msg.msg_controllen);
		printf("client buf is %c\n", iobuf[0]);
		struct cmsghdr *cmsg = CMSG_FIRSTHDR(&msg);
		if (cmsg == NULL) {
			perror("cmsg is null");
		} else {
			int receive_fd = *(int *)CMSG_DATA(cmsg);
			char buf[1024];
			if (read(receive_fd, buf, 1024) == -1)
			{
				perror("read");
			} else {
				printf("read from receive fd:%s\n", buf);
			}
			close(receive_fd);
		}
	}
	close(fd);
	return 0;
}
