/******************************************************************************
 Dijkstra's Algorithm
 ## what's the problem to be solved?
 find the shortest path of tow nodes in a graph

 ## description
 Let the node at which we are starting be called the `initial node`
 Let the `distance of node Y` be the distance from `initial node` to Y
 Dijkstra's algorithm will assign some initial distance values and will try to
 improve them step by step.
 1. Mark all node unvisited. Create a set of all the unvisited nodes called 
 the unvisited set.
 2. Assign to every node a tentative distance value: set it to zero for our 
 initial node and to infinity for all other nodes. Set the initial node as 
 current.
 3. For the current node, consider all of its unvisited neighbours and 
 calculate their tentative distances through the current node. Compare the 
 newly calculated tentative distance to the current assigned value and assign
 the smaller one. For example, if the current node A is marked with a distance 
 of 6, and the edge connecting it with a neighbour B has length 2, then the 
 distance to B through A will be 6 + 2 = 8. If B was previously marked with a 
 distance greater than 8 then change it to 8. Otherwise, the current value 
 will be kept.
 4. When we are done considering all of the unvisited neighbours of the 
 current node, mark the current node as visited and remove it from the 
 unvisited set. A visited node will never be checked again.
 5. If the destination node has been marked visited (when planning a route 
 between two specific nodes) or if the smallest tentative distance among the 
 nodes in the unvisited set is infinity (when planning a complete traversal; 
 occurs when there is no connection between the initial node and remaining 
 unvisited nodes), then stop. The algorithm has finished.
 6. Otherwise, select the unvisited node that is marked with the smallest 
 tentative distance, set it as the new "current node", and go back to step 3

 ## pesudo code
 ```
    function Dijkstra(Graph, source):

     create vertex set Q

     for each vertex v in Graph:             
         dist[v] ← INFINITY                  
         prev[v] ← UNDEFINED                 
         add v to Q                      
     dist[source] ← 0                        
     
     while Q is not empty:
         u ← vertex in Q with min dist[u]    
                                             
         remove u from Q 
         
         for each neighbor v of u:           // only v that are still in Q
             alt ← dist[u] + length(u, v)
             if alt < dist[v]:               
                 dist[v] ← alt 
                 prev[v] ← u 

     return dist[], prev[] 
  ```

 ## intuitive proof
 find shortest path from s to w
 Dijkstra's algorithm works by marking one vertex at a time as it discovers a 
 shortest path to that vertex. 

 Initially we mark just the vertex s since we know its (trivial) shortest path 
 from s. 
 
 Dijkstra relies on a key idea from dynamic progamming. 

 Consider the shortest path from s to some vertex w. It traverses a sequence of
 vertices, with a final vertex v that then connects to w to finish the shortest
 path to w. The key observation is that the path up to v must be a shortest 
 path to v. Because if it weren't, we could replace that path to v with a 
 shorter path, which would also shorten the path to w, which would contradict 
 the claim that we have a shortest path to w.

 ## proof
 Invariance: for every node X in the visited set, the stored value is the 
 shortest path

 ## Running time
 
 *****************************************************************************/
#include <stdio.h>
#define N 9

void
dijkstra(int tree[][], int s, int e, int dist[], int prev[])
{
	
	for (int i = 0; i < N; i++)
		dist[i] = -1;
}

int
main(int argc, char *argv[])
{
	int tree[9][9] = {};
}

