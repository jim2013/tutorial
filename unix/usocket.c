#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <wait.h>

int
main(int argc, char *argv[])
{
	int fds[2];
	if (socketpair(AF_UNIX, SOCK_STREAM, 0, fds) != 0)
	{
		perror("fail to create socketpair");
		return 1;
	}

	pid_t pid = fork();
	if (pid < 0)
	{
		perror("fail to fork");
		return 1;
	}
	if (pid > 0)
	{
		/*parent process*/
		if (write(fds[1], "hi!", 3) == -1)
		{
			perror("fail to write");
			return 1;
		}
		/*wait for the child process terminatation*/
		wait(NULL);
	} else {
		/*child process*/
		sleep(1);
		char buf[4] = {0};
		if (read(fds[0], buf, 3) == -1)
		{
			perror("fail to read");
			return 1;
		}
		printf("receive msg:%s\n", buf);
	}
	
}
