#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
	int fd = open("exercise_3.6.c", O_RDWR|O_APPEND);
	if (fd == -1)
	{
		printf("fail to open file.\n");
		return -1;
	}

	if (lseek(fd, 0, SEEK_SET) == -1)
	{
		printf("error when lseek.\n");
		return -1;
	}
	char buf[16] = {'0'};
	if (read(fd, buf, 8) == -1){
		printf("error when read.\n");
		return -1;
	} else {
		printf("read:%s", buf);
	}
	if (write(fd, "//test\n", 7) == -1)
	{
		printf("error when write.\n");
		return -1;
	}
	return 0;
}
//test
