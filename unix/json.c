#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

int
parseJSON(char *data, size_t size)
{
	size_t i = 0;
	/* 1: object, 2: array, 3: string, 4: number */
	char vt = 0;
	short int keySize = 0;
	char key[64];
	bool kvBegin;
	bool keyStart;
	bool keyEnd;
	bool valueStart;
	bool valueEnd;
	char value[64];
	short int valueSize = 0;
	while(i < size)
	{
		char ch = data[i];
//		printf("%c\n", ch);
		if (ch == '{')
		{
			/* object start */
			if (keySize == 0) 
			{
				printf("parse start.\n");
			} else {
				vt = 1;
				printf("object starts, key is %s, value is an object.\n", key);
			}
			
			vt = 0;
			kvBegin = false;
			keyStart = false;
			keyEnd = false;
			valueStart = false;
			valueEnd = false;
			memset(key, '\0', 64);
			keySize = 0;
			memset(value, '\0', 64);
			valueSize = 0;
		} else if (ch == '}') {
			/* object end*/
			if (vt == 4)
			{
				/* number value end*/
				printf("value is %s \n", value);
				valueEnd = true;
				vt = 0;
			}
			printf("object ends\n");
		} else if (ch == '"') {
			if (!kvBegin)
			{
				kvBegin = true;
				keyStart = true;
			} else {
				if (keyStart && !keyEnd)
				{	
					printf("key is %s\n", key);
					keyEnd = true;
				}
				
				if (valueStart)
				{
					if (vt == 0) 
					{
						printf("vt is 3\n");
						vt = 3;
					} else if (vt == 3) {
						printf("value is %s \n", value);
						valueEnd = true;
						vt = 0;
					} else {
						goto malformed;
					}
				}

			}
		} else if (ch == ':') {
			/*ignore*/
			valueStart = true;
		} else if (ch == ',') {
			printf("vt is %d \n", vt);
			if (vt == 4)
			{
				/* number value end*/
				valueEnd = true;
				printf("value is %s \n", value);
				vt = 0;
			}

			if (!keyStart || !keyEnd || !valueStart || !valueEnd)
			{
				goto malformed;
			}

			vt = 0;
			kvBegin = false;
			keyStart = false;
			keyEnd = false;
			valueStart = false;
			valueEnd = false;
			memset(key, '\0', 64);
			keySize = 0;
			memset(value, '\0', 64);
			valueSize = 0;
		} else {
//			printf("valueStart is %d, valueEnd is %d, vt is %d\n", valueStart, 
//					valueEnd, vt);
			if (keyStart && !keyEnd)
			{
				key[keySize++] = ch;
			} 
			if (valueStart && !valueEnd)
			{
				if (vt == 0)
				{
					if (ch == ' ')
					{
						/* ignore */
					} else {
						if (ch >= '0' || ch <= '9' || ch == '.') 
						{
							vt = 4;
						} else {
							goto malformed;
						}
					}
				}

				if (vt == 3)
				{
					value[valueSize++] = ch;
				} else if (vt == 4) {
					if (ch >= '0' || ch <= '9' || ch == '.') 
					{
						value[valueSize++] = ch;
					} else if (ch == ' ') {
						valueEnd = true;
					}
				}
			}
		}

		i++;
	}

	return 0;

	malformed:
	{
		printf("malformed near:");
		for (int j = 0; i + j < size && j < 10; j++)
		{
			printf("%c", data[i + j]);
		}
	}
}

int
main(int argc, char *argv[])
{
	char *data = "{\"a\":11,\"b\":2, \"c\": \"hell json\", \
		\"d\":{\"d0\":1.2}}"; 
	parseJSON(data, strlen(data));
}
