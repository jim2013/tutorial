#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>

void error(const char *msg)
{
    perror(msg);
    exit(1);
}

int
main(int argc, char *argv[])
{
     socklen_t clilen;
     int n;
	 //创建一个服务端的socket
     int serverfd = socket(AF_INET, SOCK_STREAM, 0);
     if (serverfd < 0) 
        error("ERROR opening socket");
	 //初始化服务端地址
     struct sockaddr_in serv_addr;
     bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = INADDR_ANY;
	 //端口号使用9002
     serv_addr.sin_port = htons(9002);	
	//绑定服务端地址
     if (bind(serverfd, (struct sockaddr *) &serv_addr,
              sizeof(serv_addr)) < 0) error("ERROR on binding");
	 else printf("server start, listen on localhost:9002\n");
	 //等待客户端连接
     listen(serverfd, 5);
	 //客户端地址
	 struct sockaddr_in cli_addr;
     clilen = sizeof(cli_addr);
	 //接受客户端连接
     int clientfd = accept(serverfd, (struct sockaddr *) &cli_addr, &clilen);
     if (clientfd < 0) 
          error("ERROR on accept");
     char buffer[1024] = {0};
	 //读取客户端发送的信息
     n = read(clientfd, buffer, 1023);
     if (n < 0) error("ERROR reading from socket");
     printf("receive:\n%s\n",buffer);
     close(serverfd);
     close(clientfd);
     return 0; 
}
