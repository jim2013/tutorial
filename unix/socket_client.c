/*
 * The steps involved in establishing a socket on the client side are as follows:

    1. Create a socket with the socket() system call
    2. Connect the socket to the address of the server using the connect() system call
    3. Send and receive data. There are a number of ways to do this, but the simplest is to use the read() and write() system calls.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <time.h>

void print_current_time();
void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    printf("client is start....");
    int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent *server;

    if (argc < 3) {
       fprintf(stderr,"usage %s hostname port\n", argv[0]);
       exit(0);
    }
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
    printf("Please enter the message: ");
    char buffer[256] = "hi~";
    fgets(buffer,255,stdin);
    n = write(sockfd,buffer,strlen(buffer));
    if (n < 0) 
         error("ERROR writing to socket");
	while(1){
		bzero(buffer,256);
		n = read(sockfd,buffer,1);
		if (n < 0) 
			 error("ERROR reading from socket");
		printf("receive %s\n",buffer);
		print_current_time();
	}
    close(sockfd);
    return 0;
}

void print_current_time()
{
	struct timespec ts;
	if (clock_gettime(CLOCK_REALTIME, &ts) == 0)
	{
		printf("current time:%lld.%.9ld\n", (long long)ts.tv_sec, ts.tv_nsec);
	} else {
		printf("fail to get current time.\n");
	}
}
