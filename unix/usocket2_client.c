#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <unistd.h>

int
main(int argc, char *argv[])
{
	int fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd < 0)
	{
		perror("socket");
		exit(1);
	}
	struct sockaddr_un servAddr;
	servAddr.sun_family = AF_UNIX;
	strcpy(servAddr.sun_path, "my_unix_domain");
	int addrLen = offsetof(struct sockaddr_un, sun_path) 
		+ strlen(servAddr.sun_path) + 1;
	if (connect(fd, (struct sockaddr *)&servAddr, addrLen) < 0)
	{
		perror("connect");
		exit(1);
	}
	printf("connect to server\n");
	char buf[256] = "hi~";
	int n = write(fd, buf, strlen(buf));
	if (n < 0)
	{
		perror("write");
		exit(1);
	}
	close(fd);
	return 0;
}
