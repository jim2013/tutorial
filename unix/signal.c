#include<stdio.h>
#include<sys/wait.h>
#include<string.h>
#include<stdlib.h>
#include<unistd.h>

/**
 * ## what are signals
 * signals are software interrupts.
 * signals are a technique to notify a process that some condition has occured.
 * or we can say signals provide a way of handling asynchronous events.
 * For example, a user at a terminal typing the interrupt key to stop a program
 *
 * ## what can the process deal with the process
 * 1. Ignore the signal
 * 2. Let the default action occur
 * 3. Provided a function that is called when the signal occurs
 * 
 * ## signal sets
 * The signal set(sigset_t) data type reprents multiple signals, we can 
 * manipulate signal sets with the follwing five functions:
 * #include<signal.h>
 * int sigemptyset(sigset_t *set);
 * int sigfillset(sigset_t *set);
 * int sigaddset(sigset_t *set, int signo);
 * int sigdelset(sigset_t *set, int signo);
 * int sigismember(const sigset *set, int signo);
 *
 */

/* our signal-catching function*/
static void sig_int(int );

int main(void)
{
	char buf[1024];
	pid_t pid;
	int status;
	
	/**
	 * register user defined handler function for signal
	 * prototype is:
	 * #include <signal.h>
	 * void (*signal(int signo, void (*func)(int)))(int)
	 *
	 * @param signo:the signal name(or we say siganl no) of the signal
	 * @param func:the address of a function to be called when the signal 
	 * occurs, the function takes a signal integer argumen and return nothing
	 * @return: the pointer to the previous signal hander function
	 *
	 */
	if (signal(SIGINT, sig_int) == SIG_ERR)
	printf("signal error");
	printf("%% "); /* print prompt (printf requires %% to print %) */
	while (fgets(buf, 1024, stdin) != NULL) {
		if (buf[strlen(buf) - 1] == '\n')
			buf[strlen(buf) - 1] = 0; /* replace newline with null */
		if ((pid = fork()) < 0) {
			printf("fork error");
		} else if (pid == 0) {
			/* child */
			execlp(buf, buf, (char *)0);
			printf("couldn’t execute: %s", buf);
			exit(127);
		}
		/* parent */
		if ((pid = waitpid(pid, &status, 0)) < 0)
			printf("waitpid error");
		printf("%% ");
	}
	exit(0);
}

void sig_int(int signo)
{
	printf("interrupt\n%% ");
}
