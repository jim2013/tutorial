* `inet_pton` - convert IPv4 and IPv6 addresses from binary to text form
p stands for presentation
n stands for numeric

* `ntohs` - converts the unsigned short integer netshort from network byte 
order to host byte order
n stands for numeric
h stands for host
s stands for short

* CR: carriage return
* LF: line feed
