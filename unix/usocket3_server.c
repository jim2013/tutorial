#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <errno.h>
#include <string.h>   //strcpy
#include <stddef.h>	//offsetof
#include <unistd.h>

/**
 * demostrate useage of Linux abstract unix domain
 */
int
main(int argc, char *argv[])
{
	int fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (fd == -1)
	{
		perror("fail to create socket");
		return 1;
	}

	struct sockaddr_un addr;
	memset(&addr, 0, sizeof(struct sockaddr_un));
	addr.sun_family = AF_UNIX;
	memcpy(addr.sun_path + 1, "aaa", 3);
	int addrLen = sizeof(struct sockaddr_un);
	if (bind(fd, (struct sockaddr*)&addr, addrLen) == -1)
	{
		perror("fail to bind");
		return 1;
	}
	if (listen(fd, 10) == -1)
	{
		perror("listen");
		return 1;
	}

	struct sockaddr_un clientAddr;
	memset(&addr, 0, sizeof(struct sockaddr_un));
	socklen_t clientLen = sizeof(clientAddr);
	int clientFD = accept(fd, (struct sockaddr *)&clientAddr, &clientLen);
	if (clientFD == -1)
	{
		perror("accept");
		return 1;
	} else {
		for (int i = 1; ; i++)
		{
			if (clientAddr.sun_path[i] == '\0')
				break;
			printf("%c", clientAddr.sun_path[i]);
		}
		printf("\n");
		printf("accept path:%s\n", clientAddr.sun_path);
	}
	char buf[256] = {0};
	int n = read(clientFD, buf, 256);
	if (n < 0)
	{
		perror("read");
		return 1;
	}
	printf("receive msg:%s\n", buf);
	close(fd);
	close(clientFD);
}
