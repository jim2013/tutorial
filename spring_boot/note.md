* run1 : mvn spring-boot:run
* run2 : mvn package, then java -jar target/xxx.jar

## build system
* Dependecy Management: each release of spring boot provides a curated list of
  dependencies that it supports. In practice, you do not need to provide a
version for any of these dependencies in your build configuration, as Spring
Boot manages that for you

* Starters: Starters are a set of convenient dependency descriptors that you
  can include in your application. You get a one-stop shop for all the Spring
and related technologies that you need without having to hunt through sample
code and copy-paste loads of dependency descriptors. For example, if you want
to get started using Spring and JPA for database access, include the
spring-boot-starter-data-jpa dependency in your project.  The starters contain
a lot of the dependencies that you need to get a project up and running quickly
and with a consistent, supported set of managed transitive dependencies.

### Auto-cofiguration Spring Boot auto-configuration attempts to automatically
configure your Spring application based on the jar dependencies that you have
added.  You need to opt-in to auto-configuration by adding the
@EnableAutoConfiguration or @SpringBootApplication annotations to one of your
@Configuration classes.

* using sbt with *spring-boot-starter-parent* 有哪些默认配置？ override
  individual dependencies
* using sbt without the parent pom
  仍然使用spring-boot-starter-parent的dependency manager
* spring boot maven plugin

### Auto-config
* 查看当前auto-config :If you need to find out what auto-configuration is
  currently being applied, and why, start your application with the --debug
switch. Doing so enables debug logs for a selection of core loggers and logs a
conditions report to the console.
* Disabling Specific Auto-configuration Classes

### developer tools 具体有什么工具？
* Developer tools are automatically disabled when running a fully packaged
  application.
* property defaults
* automatic restart
* LiveReload : trigger a browser refresh
* Global Setting: Any properties added to this file apply to all Spring Boot
  applications on your machine that use devtools
* Remote Application : 远程

## Spring Boot Feature

## Externalized Configuration
1. You can use properties files, YAML files, environment variables, and
command-line arguments to externalize configuration
2. Property values can be injected directly into your beans by using the @Value
annotation, accessed through Spring’s Environment abstraction, or be bound to
structured objects through @ConfigurationProperties
Q:已经有了application.propertis文件，为何还需要application-{profile}.properties
Q:If you have specified any files in spring.config.location, profile-specific
variants of those files are not considered. Use directories in
spring.config.location if you want to also use profile-specific properties.???
### Application Property Files SpringApplication loads properties from
application.properties files in the following locations and adds them to the
Spring Environment:
1. A /config subdirectory of the current directory
2. The current directory
3. A classpath /config package
4. The classpath root
* spring.config.name and spring.config.location are used very early to
  determine which files have to be loaded ### Profile-specific Properties ###
Using YAML Instead of Properties ### Type-safe Configuration
Properties(@ConfigurationProperties)
* Third-party Configurations 是什么意思？ ### Profiles Spring Profiles provide
  a way to segregate parts of your application configuration and make it be
available only in certain environments. Any @Component or @Configuration can be
marked with @Profile to limit when it is loaded, as shown in the following
example: ```java @Configuration @Profile("production") public class
ProductionConfiguration {

	// ...

} ```

## 参考资料
* [SpringBoot Doc](https://docs.spring.io/spring-boot/docs/2.1.3.RELEASE/reference/htmlsingle/)
* [SpringBoot官文笔记](http://xbynet.top/2018/01/09/SpringBoot%E5%AE%98%E6%96%87%E7%AC%94%E8%AE%B0/)
