package song.stream;

import java.util.stream.Collectors;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.TreeSet;
import java.util.Set;
import java.util.Map;

class Article {
	private String id;
	private String title;
	private String tag;

	public Article(String id, String title, String tag){
		this.id = id;
		this.title = title;
		this.tag = tag;
	}

	public String getId(){
		return this.id;
	}

	public String getTitle(){
		return this.title;
	}

	public String getTag(){
		return this.tag;
	}
}

public class CollectDemo{
	private Collection<Article> articles = new ArrayList<>();
	public static void main(String[] args) {
		CollectDemo demo = new CollectDemo();
		demo.init();
		demo.toCollection();
		demo.groupBy();
		demo.groupByAndMapping();
	}	

	private void toCollection() {
		List<String> ids = articles.stream()
							.map(Article::getId)
							.collect(Collectors.toList());
		Set<String> tags = articles.stream()
								.map(Article::getTag)
								.collect(Collectors.toCollection(TreeSet::new));
		System.out.println("***article id*****");
		ids.forEach(id -> System.out.println(id));
		System.out.println("***article tag****");
		tags.forEach(id -> System.out.println(id));
	}

	private void groupBy() {
		Map<String, List<Article>> tagArticleMap = articles.stream()
										.collect(Collectors.groupingBy(Article::getTag));
		System.out.println("***tag to articles map***");
		tagArticleMap.forEach((k, v) -> System.out.println(k + ":" + v));
	}

	private void groupByAndMapping() {
		Map<String, List<String>> tagTitleMap = articles.stream()
											.collect(Collectors.groupingBy(Article::getTag, Collectors.mapping(Article::getTitle, Collectors.toList())));
		System.out.println("***tag to titles map***");
		tagTitleMap.forEach((k, v) -> System.out.println(k + ":" + v));
	}
	
	private void init(){
		Article a1 = new Article("1", "learn java", "java");
		Article a2 = new Article("2", "learn java stream", "java");
		Article a3 = new Article("3", "learn c", "c");
		Article a4 = new Article("4", "learn python", "python");
		articles.add(a1);
		articles.add(a2);
		articles.add(a3);
		articles.add(a4);
	}
}
