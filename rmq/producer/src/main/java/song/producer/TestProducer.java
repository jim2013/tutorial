package song.producer;

import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.client.producer.SendResult;
/**
 * compile from suorce:mvn -Prelease-all -DskipTests clean install -U
 * cd distribution/target/rocketmq-4.6.0-SNAPSHOT/
 * nohup ./bin/mqnamesrv &
 * nohup ./bin/mqbroker -n localhost:9876 &
 * @author song
 *
 */
public class TestProducer{
	public static void main(String[] args) throws Exception{
		System.out.println("rocket mq producer demo start....");
		String groupName = "group1";
		DefaultMQProducer p = new DefaultMQProducer(groupName);
		String nameSrv = "localhost:9876";
		p.setNamesrvAddr(nameSrv);
		p.start();

		String body = "hello rocket mq1";
		if (args.length >= 2){
			body = args[1];
		}	

		String topic = "topic19";
		String tag = "tag1";
		Message msg = new Message(topic, tag, body.getBytes("utf-8"));
		SendResult ret = p.send(msg);
		System.out.println("send result :" + ret);

		p.shutdown();
		System.out.println("rocket mq producer demo finish....");
	}

}
