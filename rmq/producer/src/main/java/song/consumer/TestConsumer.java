package song.consumer;

import java.util.List;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeOrderlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerOrderly;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.apache.rocketmq.common.message.MessageExt;

public class TestConsumer {
	public static void main(String[] args) throws MQClientException, InterruptedException{
		String groupName = "group2";
		DefaultMQPushConsumer consumer = new DefaultMQPushConsumer(groupName);
		consumer.setConsumeFromWhere(ConsumeFromWhere.CONSUME_FROM_FIRST_OFFSET);
		String namesrvAddr = "localhost:9876";
		consumer.setNamesrvAddr(namesrvAddr);
		String topic = "topic17";
		consumer.subscribe(topic, "tag1");
		consumer.registerMessageListener(new MessageListenerOrderly(){

			@Override
			public ConsumeOrderlyStatus consumeMessage(List<MessageExt> msgs, ConsumeOrderlyContext context) {
				context.setAutoCommit(false);
				System.out.println("receive msg:" + msgs);
				return ConsumeOrderlyStatus.SUCCESS;
			}
			
		});
		consumer.start();
        System.out.println("Consumer Started.");
		
		Thread.sleep(2000);
	}
}
