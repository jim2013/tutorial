#!/bin/python3

d1=2
d2=4
d3=2

def s(s0, s1, s2, s3):
    s4 = d3*(s1-s0) + d2*(s2-s1) + d1*(s3-s2) + s3
    return s4

sum=[1]
s3=0
for i in range(1, 100):
    s0=0
    s1=0
    s2=0
    s3 = sum[i-1]
    if i > 1:
        s2 = sum[i-2]
    if i > 2:
        s1 = sum[i-3]
    if i > 3:
        s0 = sum[i-4]
    s4 = s(s0, s1, s2, s3)
    sum.append(s4)
    print('day %d: %d' % (i, s4))
    if s4 > 20000000:
        break
