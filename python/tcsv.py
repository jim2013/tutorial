#!/bin/python3

# doc:https://docs.python.org/3.7/library/csv.html#id3

import csv

fieldnames = ['key1', '时间', '更新']
with open('t.csv', 'w', newline='' ) as csvfile:
	writer = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
	writer.writerow(fieldnames)
	writer.writerow(['a,\nb,c', '2018-08-01'])
	writer.writerow(['a"', '2018-07-01'])
	writer.writerow(['b', '2017-01-01', '2018-07-00'])
#   使用dirctWriter时,wirterow的参数是一个map,对于从数据库中读，不方便
#	writer = csv.DictWriter(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
#			fieldnames=fieldnames)
#	writer.writerow({'key1':'1'})

with open('t.csv', newline='') as csvfile:
	reader = csv.reader(csvfile, delimiter=',', quotechar='"')
	for row in reader:
		print(', '.join(row))

with open('t.csv', newline='') as csvfile:
	reader = csv.DictReader(csvfile, delimiter=',', quotechar='"')
	print(reader.fieldnames)
	for row in reader:
		print(row[fieldnames[0]])
