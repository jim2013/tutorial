#!/bin/python3

import requests
import json
import os
import sys

def getFilename(albumId, title, audioUrl):
    i = audioUrl.rfind('.')
    suffix = '.m4a'
    if i > -1:
        suffix = audioUrl[i:]
    return str(albumId) + '/' + title + suffix

URL_TRACKS = 'https://www.ximalaya.com/revision/album/v1/getTracksList'
URL_AUDIO = 'https://www.ximalaya.com/revision/play/v1/audio'
def getTracks(albumId, pageNum, pageSize):
    payload = {'albumId':albumId, 'pageNum':pageNum, 'sort':0, 'pageSize':pageSize}
    headers = {'user-agent': 'mhttp', 'accept': '*/*'}
    r = requests.get(URL_TRACKS, params=payload, headers=headers)
    if r.status_code != 200:
        print("fail to get tracks, status code: %d" % r.status_code)
        return
    r.encoding='utf-8'
    tracksInfo = r.json()
    if tracksInfo['ret'] != 200:
        print("fail to get tracks, tracksInfo.ret: %d" % tracksInfo.ret)
    trackCount = 0
    for track in tracksInfo['data']['tracks']:
        trackCount = trackCount + 1
        trackId = track['trackId']
        title = track['title']
        audioInfoResp = requests.get(URL_AUDIO, params={'id':trackId, 'ptype':1}, headers=headers)
        if audioInfoResp.status_code != 200:
            print("fail to get audio info, status code: %d" % audioInfoResp.status_code)
            continue
        audioInfo = audioInfoResp.json()
        if audioInfo['ret'] != 200:
            print("fail to get audio info, audioInfo.ret is %d" % auidoInfo['ret'])
        audioUrl = audioInfo['data']['src']
        audioFileResp = requests.get(audioUrl, stream = True)
        if audioFileResp.status_code != 200:
            print("fail to get audio file, status code: %d" % audioFileResp.status_code)
            continue
        fn = getFilename(albumId, title, audioUrl)
        if os.path.exists(fn):
            print("already exists, skip %s" % fn)
            continue
        else:
            print("download %s" % fn)
        with open(fn, 'wb') as f:
            for chunk in audioFileResp.iter_content(chunk_size = 16 * 1024):
                f.write(chunk)

    return trackCount


albumId = 353331
if not os.path.exists(str(albumId)):
    # create directory
    os.mkdir(str(albumId))
elif not os.path.isdir(str(albumId)):
    print('%d is not a directory.')
    sys.exit()
pageNum = 1
pageSize = 30
trackCount = pageSize
while trackCount == pageSize:
    trackCount = getTracks(albumId, pageNum, pageSize)
    pageNum = pageNum + 1
