#!/bin/python3

# [digitalocean how to use args and kwargs](https://www.digitalocean.com/community/tutorials/how-to-use-args-and-kwargs-in-python-3)

def multiple(*args):
    z = 1
    for arg in args:
        z = z * arg
    return z

def multiple1(x, y, z):
    return x*y*z

def f(v,func, *args):
    print(args)
    if func(v, *args):
        print('true')
    else:
        print('false')
    return

print(multiple(2, 3))
print(multiple(2, 3, 4))
# *args can also be used in funciton call
l = [2, 3, 4]
print(multiple1(*l))

s1=set()
s1.add('123')
s1.add('abc')
f('123', lambda v, s:v in s, s1)
f('1234', lambda v, s:v in s, s1)
s2=set()
s2.add('abc')
s2.add('edf')
f('123', lambda v, s1, s2: v in s1 and v in s2, s1, s2)
f('abc', lambda v, s1, s2: v in s1 and v in s2, s1, s2)
f('abc', lambda v: v != '0.00')
