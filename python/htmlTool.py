#!/bin/python3

from html.parser import HTMLParser
from html.entities import name2codepoint
import requests

# 亲亲小说网在线阅读解析出分章标题和内容
class QQXSWParser(HTMLParser):
    def __init__(self):

        # 0:初始状态
        # 1:标题div, 2:标题h2 
        # 10:正文
        # 20 下一页
        self.stage=0
        self.title=''
        self.content=''
        self.nextChapterUrl=''
        HTMLParser.__init__(self)
    def handle_starttag(self, tag, attrs):
#        print("Start tag:", tag)
#        for attr in attrs:
#            print("     attr:", attr)
        if tag == 'div':
            for attr in attrs:
                if attr[0] == 'class' and attr[1] == 'chapter_title':
                    self.stage = 1
                    break
                elif attr[0] == 'id' and attr[1] == 'inner fdd':
                    self.stage = 10
                    break
        elif tag == 'h2' and self.stage == 1:
            self.stage = 2
        elif tag == 'a':
            nextUrl = ''
            for attr in attrs:
                if attr[0] == 'id' and attr[1] == 'pager_next':
                    self.stage = 20
                if attr[0] == 'href':
                    nextUrl = attr[1]
            if self.stage == 20:
                self.nextChapterUrl = nextUrl

    def handle_endtag(self, tag):
#        print("End tag  :", tag)
        if tag == 'div' and self.stage == 10:
            self.stage = 0
    def handle_data(self, data):
#        print("Data     :", data)
        if self.stage == 2:
            self.title = data
            self.stage = 0
        elif self.stage == 10:
            self.content = self.content + data
        elif self.stage == 20:
            self.stage = 0

#    def handle_comment(self, data):
#        print("Comment  :", data)
#
#    def handle_entityref(self, name):
#        c = chr(name2codepoint[name])
#        print("Named ent:", c)
#
#    def handle_charref(self, name):
#        if name.startswith('x'):
#            c = chr(int(name[1:], 16))
#        else:
#            c = chr(int(name))
#        print("Num ent  :", c)
#
#    def handle_decl(self, data):
#        print("Decl     :", data)

def parseQQXWS():
    count = 0
    prefix = 'https://www.qinqinxsw.com'
    url = 'https://www.qinqinxsw.com/15/15781/4429714.html'
    with open('data/太平盛世.txt', 'w') as f:
        while True:
            r = requests.get(url)
            r.encoding='gb2312'
            html = r.text
            parser = QQXSWParser()
            parser.feed(html)
            f.write(parser.title)
            f.write(parser.content)
            f.write('\n====================\n')
            url = prefix + parser.nextChapterUrl
            count = count + 1;
            print(parser.title)
            if parser.title == '正文 第33 章 获奖感言':
                break;
    f.close()

if __name__ == '__main__':
    parseQQXWS()
