# Frequently Ask Question about python

## string
* len(str)
* str.replace('src', 'dest')
* char at
```python
str = 'hello world'
str[0]
str[-1]
```
* join
> `str.join(iterable)`
> Retrun a string which is the concatenation of the strings in the iterable iterable.

```python
l=['a', 'b', 'c']
str=';'.join(l) # str will be: a;b;c
```

## map
* map(function, iterable)
> Return an iterator that applies function to every item of iterable

```python
l=[1,2,3]
nl=map(lambda x:x*2, l) #nl will be:[2, 4, 6]
```

## list
* list comprehensions
> List comprehensions provide a concise way to create lists.
> make new lists where each element is the result of some operations applied
> to each member of another sequence or iterable. 
> Or to create a subsequence of those elements that satisfy a certain condition.

```python
squares = list(map(lambda x: x**2, range(10)))
# or equivalently
squares = [x**2 for x in range(10)]
```
