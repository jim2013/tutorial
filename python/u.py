#!/bin/python3
# common utils module, such as : fill, diff, module
import csv
import pymysql
import os
import itertools

def map(fname, keys, valueMaps, outfname):
    "map field value to new value"
    with open(fname) as f, open(outfname, 'w') as outf:
        reader = csv.DictReader(f, delimiter=',', quotechar='"')
        fieldnames = reader.fieldnames
        writer = csv.DictWriter(outf, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
                fieldnames=fieldnames)
        writer.writeheader()
        for row in reader:
            for k, valueMap in zip(keys, valueMaps):
                if k not in row:
                    continue
                oldValue = row[k]
                if oldValue not in valueMap:
                    continue
                row[k] = valueMap[oldValue]
            writer.writerow(row)
    return

def map1(fn, key, func):
    fn_out = fn + ".tmp"
    with open(fn) as f, open(fn_out, 'w') as outf:
        reader = csv.reader(f)
        writer = csv.writer(outf)
        fields = reader.__next__()
        writer.writerow(fields)
        keyIndex = fields.index(key)
        if key not in fields:
            print("can not find key from fields.")
            return
        for row in reader:
            newValue = func(row[keyIndex])
            row[keyIndex] = newValue
            writer.writerow(row)
    os.rename(fn_out, fn)

def reorderColumn(fn, newTitles, fn_out = None):
    rename = False
    if fn_out is None:
        fn_out = fn + ".tmp"
        rename = True
    indexs = []
    with open(fn, 'r') as f, open(fn_out,'w') as outf:
        reader = csv.reader(f)
        writer = csv.writer(outf)
        writer.writerow(newTitles)

        titles = reader.__next__()
        indexs = []
        for title in newTitles:
            if title not in titles:
                print("can not find column: " + title)
                return
            indexs.append(titles.index(title))

        for row in reader:
            newRow = []
            for index in indexs:
                newRow.append(row[index])
            writer.writerow(newRow)
    if rename:
        os.rename(fn_out, fn)

def groupBy(fn, field, newTitles, fn_out):
    with open(fn, 'r') as f, open(fn_out, 'w') as outf:
        reader = csv.reader(f)
        titles = reader.__next__()
        fieldIndex = titles.index(field)
        fieldValue2CountMap = {}
        totalCount = 0
        for row in reader:
            totalCount = totalCount + 1
            fieldValue = row[fieldIndex]
            count = 0
            if fieldValue not in fieldValue2CountMap:
                count = 1
            else:
                count = fieldValue2CountMap[fieldValue] + 1
            fieldValue2CountMap[fieldValue] = count

        writer = csv.writer(outf)
        newTitles.append('百分比')
        writer.writerow(newTitles)
        for k, v in fieldValue2CountMap.items():
            writer.writerow([k, v, round(v * 100 / totalCount, 2)])

def appendField(fn, field, fieldValues):
    fn_out = fn + ".tmp"
    with open(fn) as f, open(fn_out, 'w') as outf:
        reader = csv.reader(f)
        writer = csv.writer(outf);
        fields = reader.__next__()
        fields.append(field)
        writer.writerow(fields)
        for (row, value) in itertools.zip_longest(reader, fieldValues):
            row.append(value)
            writer.writerow(row)
    os.rename(fn_out, fn)

def join_part1(fn1, fn2, f2joinKeys, joinTitles = None):
    sameKey = None
    with open(fn1) as f1, open(fn2) as f2:
        reader1 = csv.reader(f1)
        reader2 = csv.reader(f2)
        fields1 = reader1.__next__();
        fields2 = reader2.__next__();
        for field in fields1:
            if (field in fields2):
                sameKey = field
                break
        if sameKey is None:
            print("can not determine the same key from the two files.")
        else:
            print("same key:" + sameKey)
            fn_out = fn1 + ".tmp"
            join_part(fn1, sameKey, fn2, sameKey, f2joinKeys, fn_out, joinTitles)
            os.rename(fn_out, fn1)

def join_part(f1name, f1key, f2name, f2key, f2joinKeys, outfname, joinTitles = None):
    "part join by f1key and f2keys"
    with open(f1name) as f1, open(f2name) as f2, open(outfname, 'w') as outf:
        reader1 = csv.reader(f1)
        reader2 = csv.DictReader(f2)
        fieldnames = reader1.__next__()
        joinIndex = fieldnames.index(f1key)
        if joinTitles is not None:
            for k in joinTitles:
                fieldnames.insert(joinIndex + 1, k)
        else:
            for k in f2joinKeys:
                fieldnames.insert(joinIndex + 1, k)
        writer = csv.writer(outf)
        writer.writerow(fieldnames)
        joinValues = {}
        for row in reader2:
            if f2key not in row:
                continue
            values = []
            for joinK in f2joinKeys:
                if joinK in row:
                    values.append(row[joinK])
                else:
                    values.append('未知')
            joinValues[row[f2key]] = values    

        for row in reader1:
            f1value = row[joinIndex]
            if f1value not in joinValues:
                for t in f2joinKeys:
                    row.insert(joinIndex + 1, '未知')
            else:
                tmp = 0
                for joinValue in joinValues[f1value]:
                    row.insert(joinIndex + 1, joinValue)
            writer.writerow(row)
    return

def filter1(fname, key, func):
    outfname = fname + ".tmp";
    filter(fname, key, func, outfname)
    os.rename(outfname, fname)


def filter(fname, key, func, outfname, *args):
    print(fname)
    with open(fname) as f, open(outfname, 'w') as outf:
        reader = csv.DictReader(f)
        writer = csv.DictWriter(outf, fieldnames=reader.fieldnames)
        writer.writeheader()
        count = 0;
        for row in reader:
            if key in row and func(row[key], *args):
                count = count+1
                writer.writerow(row)
    return count

def union_field(fname, keys, union_field_name, outfname):
    with open(fname, 'r') as f, open(outfname, 'w') as outf:
        reader = csv.reader(f)
        titles = reader.__next__()
        key_indexes = []
        for key in keys:
            key_indexes.append(titles.index(key))
        writer = csv.writer(outf)
        new_titles = titles
        new_titles.append(union_field_name)
        writer.writerow(new_titles)
        for row in reader:
            union_value = ''
            for key_index in key_indexes:
                union_value = union_value + row[key_index].strip() + '_'
            row.append(union_value)
            writer.writerow(row)

def distinct(fname, key, outfname):
    with open(fname, 'r') as f, open(outfname, 'w') as outf:
        reader = csv.reader(f)
        title = reader.__next__()
        index = title.index(key)
        writer = csv.writer(outf)
        writer.writerow(title)
        existValues = []
    for row in reader:
        value = row[index]
        if value in existValues:
            continue;
        existValues.append(value)
        writer.writerow(row)
            

def diff(f1name, f1key, f2name, f2key, outf1name, outf2name):
    s1 = set()
    s2 = set()
    with open(f1name) as f1, open(f2name) as f2:
        reader1 = csv.DictReader(f1)
        for row in reader1:
            if f1key in row:
                s1.add(row[f1key].strip())
        reader2 = csv.DictReader(f2)
        for row in reader2:
            if f2key in row:
                s2.add(row[f2key].strip())
    d1 = s1 - s2
    print(d1)
    filter(f1name, f1key, lambda val, s : val in s, outf1name, d1)
    d2 = s2 - s1
    print(d2)
    filter(f2name, f2key, lambda val, s : val in s, outf2name, d2)
    return

def processColumn(f1name, columnName, f2name, func, newColumnName = None):
    with open(f1name, 'r') as f1, open(f2name, 'w') as f2:
        reader = csv.reader(f1);
        writer = csv.writer(f2);
        firstRow = True
        columnIndex = -1
        for row in reader:
            if firstRow:
                columnIndex = row.index(columnName)
                if newColumnName is not None:
                    row[columnIndex] = newColumnName
                firstRow = False
            else:
                row[columnIndex] = func(row[columnIndex])
            writer.writerow(row)

def getColumnData(filename, column):
    "get column data of csv file"
    data = []
    with open(filename) as f:
        reader = csv.reader(f)
        firstrow = True
        for row in reader:
            if firstrow:
                firstrow = False
                continue
            if len(row) > column:
                data.append(row[column])
    return data

def getColumnData1(filename, columnName):
    "get column data of csv file"
    data = []
    with open(filename) as f:
        reader = csv.reader(f)
        firstrow = True
        columnIndex = 0
        for row in reader:
            if firstrow:
                firstrow = False
                columnIndex = row.index(columnName)
                continue
            data.append(row[columnIndex])
    return data

def getMapData(filename, keyTitle, valueTitle):
    data = {}
    with open(filename, 'r') as f:
        reader = csv.DictReader(f)
        for row in reader:
            data[row[keyTitle]] = row[valueTitle]
    return data

def field2Title(fields, titleMap):
    title = []
    for field in fields:
        if field in titleMap:
            title.append(titleMap[field])
        else:
            title.append(field)
    return title

def getSql(fields, condition, tableName, referKey, size):
    sql = 'select ' + ','.join(fields) + ' from ' + tableName + ' where ' + referKey + '> %d'
    if condition is not None and condition != '':
        sql = sql + ' and ' + condition
    sql = sql + ' order by ' + referKey + ' asc limit ' + str(size)
    return sql

# [python database api specification v2.0](https://www.python.org/dev/peps/pep-0249/)
def getDBConn(db):
    "get a db connection"
    if db == 'audiobook':
        return pymysql.connect(host="81.69.166.161", user="audiobook_jump_r@aYheFPWQp3nEICzP", password="BrT34RWkm8tbayIstlr1uhLDvOkq2bZV"
            , port=8306, charset='utf8')
    elif db == 'platform':
        return pymysql.connect("plus-r.mysql.lrts.me", "platform_read", \
                "Ycnsr,,R02.zsr:#C4]Rin:sri8")
    elif db == 'readbook':
        return pymysql.connect("plus-r.mysql.lrts.me", "readbook_r", \
                "hnzrjgg@#9248zrwliGUnz9024..RIbT")
    elif db == 'earth':
        return pymysql.connect("172.16.6.2", "lazyaudio", \
                "q(nQ5_0xeYNriZaUSBvgP)@E")
    elif db == 'billing':
        return pymysql.connect("plus-r.mysql.lrts.me", "billing_r", \
                "NKwiwcnwi#Cnmwi!]Rinwui==Unciwg9248gwbw82u")
    elif db == 'yyting_plus':
        return pymysql.connect("plus-r.mysql.lrts.me", "yyting_plus_r", \
                'YhnaHnzm2824@Cn,Ri[Ri:sriGnwi824G')
    elif db == 'cold_data':
        return pymysql.connect("192.168.1.218", "cold_data_r", \
                'Rynzm899248,.Ri!lCuiwg9Oj28R')
    else:
        return None

def sumColumn(inf, columnName):
    columns = getColumnData1(inf, columnName)
    total = 0.0
    for column in columns:
        total = total + float(column)
    return total

def loadFromDB(db, tableName, filename):
    conn = getDBConn(db)
    sql = "select * from `%s`.`%s`" % (db, tableName)
    cursor = conn.cursor()
    cursor.execute(sql)
    rets = cursor.fetchall()

    title = []
    for desc in cursor.description:
        title.append(desc[0])
    with open(filename, 'w') as f:
        writer = csv.writer(f)
        writer.writerow(title)
        for ret in rets:
            writer.writerow(ret)
    conn.close()
    return

def loadFromDB1(db, sql, filename, title = [], appendFile = False, conn = None):
    myConn = False
    if conn is None:
        conn = getDBConn(db)
        myConn = True
    cursor = conn.cursor()
    cursor.execute(sql)
    rets = cursor.fetchall()

    if len(title) == 0:
        for desc in cursor.description:
            title.append(desc[0])
    openflag = 'w'
    if appendFile:
        openflag = 'a'
    with open(filename, openflag) as f:
        writer = csv.writer(f)
        writer.writerow(title)
        for ret in rets:
            writer.writerow(ret)
    if myConn: 
        conn.close()
    return

def loadFromDBPageByPage(db, sql, referKey, filename, title = [],
        appendFile=False, conn = None, referId=0):
    "根据referKey分页，瀑布流遍历所有数据"
    debug = True
    myConn = False
    if conn is None:
        print("open conn")
        conn = getDBConn(db)
        myConn = True
    cursor = conn.cursor()
    openFlag = 'w'
    if appendFile:
        openFlag = 'a'
    f = open(filename, openFlag)
    w = csv.writer(f)
    firstPage = True
    findReferKey = False
    referKeyIndex = 0 
    while True:
        referId = int(referId)
        if debug:
            print("referId is: %d" % referId)
        cursor.execute(sql % referId)
        rets = cursor.fetchall()

        if firstPage:
            firstPage = False
            if len(title) == 0:
                for desc in cursor.description:
                    title.append(desc[0])
            if not appendFile:
                w.writerow(title)
            
            for desc in cursor.description:
                if desc[0] == referKey:
                    findReferKey = True
                    break;
                referKeyIndex = referKeyIndex + 1
            if not findReferKey:
                print("fail to find referKey")
                break
        if len(rets) == 0:
            break
        for ret in rets:
            w.writerow(ret)
        referId = rets[len(rets) - 1][referKeyIndex]

    f.close()
    if myConn:
        conn.close()

def loadFromDBPageByPage1(db, sql, referKey, filename, title = [], tableNum = 1
        , referId=0):
    "根据referKey分页，瀑布流遍历所有数据"
    if tableNum < 1 or tableNum > 100:
        print("unsupported tableNum!");
        return
    if tableNum == 1:
        return loadFromDBPageByPage(db, sql, referKey, filename, title, 
                referId=referId)
    conn = getDBConn(db)

    for i in range(tableNum):
        tmpsql = sql
        if tableNum <= 10:
            tmpsql = sql.replace('%s', str(i), 1)
        elif tableNum <=100:
            if i < 10:
                tmpsql = sql.replace('%s', '0' + str(i), 1)
            else:
                tmpsql = sql.replace('%s', str(i), 1)
        print(tmpsql)
        loadFromDBPageByPage(db, tmpsql, referKey, filename, title, i != 0,
                conn, referId)

def loadFromDBById(db, sql, ids, filename, titles = None, tableNum = 1,
        debug = False, appendFile = False):
    "get data by id"
    # open file
    openFlag = 'w'
    if appendFile:
        openFlag = 'a'
    outf = open(filename, openFlag)
    writer = csv.writer(outf)
    writeTitle = False
    if appendFile:
        writeTitle = True
    if titles is not None and not appendFile:
        # use custom titles
        writer.writerow(titles)
        writeTitle = True
    # group id
    idGroup = [None] * tableNum
    if tableNum == 1:
        idGroup[0] = ids;
    else:
        for id in ids:
            factor = (int(float(id))) % tableNum
            tmpList = idGroup[factor]
            if tmpList is None:
                tmpList = list()
                idGroup[factor] = tmpList
            tmpList.append(id)

    # get db connection
    conn = getDBConn(db)
    cursor = conn.cursor()
    # load data from db
    data = []
    print("tableNUm is:" + str(tableNum))
    for i in range(tableNum):
        # get data by page
        pageSize = 1000
        tmpList = idGroup[i]
        if tmpList is None:
            continue
        tmpLength = len(tmpList)
        pageNum = (int)(tmpLength / pageSize)
        if tmpLength % pageSize != 0:
            pageNum = pageNum + 1
        for j in range(pageNum):
            start = j * pageSize
            end = (j + 1) * pageSize
            if end > tmpLength:
                end = tmpLength
            pageIds = tmpList[start:end]
            # get execute sql
            exeSql = sql
            if tableNum < 1:
                print("error:tableNum < 1")
                return None
            elif tableNum == 1:
                exeSql = sql % formatIds(pageIds) 
                #exeSql = sql % (', '.join(pageIds))
            elif tableNum <= 10:
                exeSql = sql % (str(i), ', '.join(pageIds))
            elif tableNum <= 100:
                if i < 10:
                    exeSql = sql % ('0' + str(i), ', '.join(pageIds))
                else:
                    exeSql = sql % (str(i), ', '.join(pageIds))
            else:
                print("error:tableNum > 100")
                return None
            if debug:
                print(exeSql)
                return
            cursor.execute(exeSql)
            rets = cursor.fetchall()
            print("rets size:" + str(len(rets)))
            if not writeTitle:
                # use db table fieldname as title
                title = []
                for desc in cursor.description:
                    title.append(desc[0])
                writer.writerow(title)
                writeTitle = True
            if debug:
                print('length of rets is %d' % len(rets))
            if rets is not None:
                for ret in rets:
                    writer.writerow(ret)

    # close file and db connection
    outf.close()
    conn.close()

def formatIds(ids):
    strIds=""
    for id in ids:
        strIds = strIds + "'" + id + "',"
    return strIds[0:len(strIds) - 1]

if __name__ == "__main__":
    print("i am common utils module")
