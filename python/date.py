#!/bin/python3
from datetime import datetime
import time

# timestamp to datetime
timestamp = 1528797322
date_time = datetime.fromtimestamp(timestamp)

# date_time to str
str_date_time = date_time.strftime("%m/%d/%Y, %H:%M:%S")
print(str_date_time)

# str to timestamp
date_time = datetime.strptime('2020-02-02 10:00:00', '%Y-%m-%d %H:%M:%S')
print(date_time.timestamp())
