#!/usr/bin/python3
# -*- coding:utf-8 -*-
import csv
import u
import copyright
import book

def filterByTime(time, start, end):
    return (time > start or time == start) and time < end

def filterByCP(cpId, cpIds):
    return cpId in cpIds

def getMadeByLRBook():
    cpIds = u.getColumnData1('data/copyright_lr.csv', '版权机构id')
    u.filter('data/book.csv', '版权机构id', filterByCP, 'data/madeByLR_book.csv',
            cpIds)

def getMadeByLRBook1():
    copyright.getLR()
    cpIds = u.getColumnData1('data/copyright_lr.csv', '版权机构id')
    u.filter('data/book4.csv', '版权机构id', filterByCP, 'data/madeByLR_book.csv',
            cpIds)

def getNewBookCount(start, end):
    cpIds = u.getColumnData1('data/copyright_lr.csv', '版权机构id')
    u.filter('data/book.csv', '添加时间', filterByTime, 'data/madeByLR_newBook.csv',
            start, end)
    u.filter('data/madeByLR_newBook.csv', '版权机构id', filterByCP, 
            'data/madeByLR_newBook1.csv', cpIds)
    bookIds = u.getColumnData('data/madeByLR_newBook1.csv', 1)
    return len(bookIds)

def getAllNewBookCount(start, end):
    u.filter('data/book.csv', '添加时间', filterByTime, 'data/madeByLR_newBook.csv',
            start, end)
    bookIds = u.getColumnData('data/madeByLR_newBook.csv', 1)
    return len(bookIds)

def getNewLength_old(start, end):
    bookIds = u.getColumnData('data/madeByLR_book.csv', 0)
    sql = 'select res_id, length, book_id from audiobook.t_resource where \
    state in (1, 2) and create_time >= "%s" and create_time <= "%s"'  % (start, end)
    sql = sql + 'and res_id > %d order by res_id asc limit 500'
    title = ['资源id', '时长(秒)', '书籍ID']
    u.loadFromDBPageByPage('audiobook', sql, 'res_id', 
            'data/madeByLR_resource_old.csv', title)
    u.filter('data/madeByLR_resource_old.csv', '书籍ID', filterByCP, 
            'data/madeByLR_resource1_old.csv', bookIds)
    return u.sumColumn('data/madeByLR_resource1_old.csv', '时长(秒)')

def getNewLength(start, end):
    bookIds = u.getColumnData('data/madeByLR_book.csv', 0)
    sql = 'select res_id, book_id from audiobook.t_resource where \
    state in (1, 2) and create_time >= "%s" and create_time <= "%s"'  % (start, end)
    sql = sql + 'and res_id > %d order by res_id asc limit 500'
    title = ['资源id', '书籍ID']
    u.loadFromDBPageByPage('audiobook', sql, 'res_id', 
            'data/madeByLR_resource.csv', title)
    fn_resource1 = 'data/madeByLR_resource1.csv'
    u.filter('data/madeByLR_resource.csv', '书籍ID', filterByCP, fn_resource1,
            bookIds)
    fn_resource2 = 'data/madeByLR_resource2.csv'
    u.union_field(fn_resource1, ['书籍ID', '资源id'], '书籍资源', fn_resource2)

    resourceIds = u.getColumnData1('data/madeByLR_resource1.csv', '资源id')
    sql = 'select book_id, resource_id, length from audiobook.t_resource_audio\
            where resource_id in (%s)'
    fn_audio = 'data/madeByLR_audio.csv'
    title = ['书籍ID', '资源id', '时长(秒)']
    u.loadFromDBById('audiobook', sql, resourceIds, fn_audio, title)
    fn_audio1 = 'data/madeByLR_audio1.csv'
    u.union_field(fn_audio, ['书籍ID', '资源id'], '书籍资源', fn_audio1)

    fn_resource3 = 'data/madeByLR_resource3.csv'
    u.join_part(fn_resource2, '书籍资源', fn_audio1, '书籍资源', 
            ['时长(秒)'], fn_resource3)

    return u.sumColumn(fn_resource3, '时长(秒)')

def getAllNewLength(start, end):
    bookIds = u.getColumnData('data/madeByLR_book.csv', 0)
    sql = 'select res_id, book_id from audiobook.t_resource where \
    state in (1, 2) and create_time >= "%s" and create_time < "%s"'  % (start, end)
    sql = sql + 'and res_id > %d order by res_id asc limit 1000'
    title = ['资源id', '书籍ID']
    u.loadFromDBPageByPage('audiobook', sql, 'res_id', 
            'data/madeByLR_resource.csv', title)
    fn_resource = 'data/madeByLR_resource.csv'
    fn_resource2 = 'data/madeByLR_resource2.csv'
    u.union_field(fn_resource, ['书籍ID', '资源id'], '书籍资源', fn_resource2)

    resourceIds = u.getColumnData1(fn_resource, '资源id')
    sql = 'select book_id, resource_id, length from audiobook.t_resource_audio\
            where resource_id in (%s)'
    fn_audio = 'data/madeByLR_audio.csv'
    title = ['书籍ID', '资源id', '时长(秒)']
    u.loadFromDBById('audiobook', sql, resourceIds, fn_audio, title)
    fn_audio1 = 'data/madeByLR_audio1.csv'
    u.union_field(fn_audio, ['书籍ID', '资源id'], '书籍资源', fn_audio1)

    fn_resource3 = 'data/madeByLR_resource3.csv'
    u.join_part(fn_resource2, '书籍资源', fn_audio1, '书籍资源', 
            ['时长(秒)'], fn_resource3)

    return u.sumColumn(fn_resource3, '时长(秒)')

def tmp():
    copyright.getLR()
    cpIds = u.getColumnData1('data/copyright_lr.csv', '版权机构id')
    u.filter('data/book4.csv', '版权机构id', filterByCP,
            'data/madeByLR_book.csv', cpIds)

#tmp()

book.getAll()
copyright.getLR()
start = '2022-03-01 00:00:00'
end = '2022-04-01 00:00:00'
newBookCount = getNewBookCount(start, end)
newLength = getNewLength(start, end)
print('%s至%s新增书籍：%d, 新增时长(小时)：%f' 
        % (start, end, newBookCount, newLength / 3600))
