#!/bin/python
import json

inFile = open("remedy.json", "r")
r = json.load(inFile)
hits = r['hits']['hits']
for hit in hits:
	print hit["_id"]

# saving structure data with json
# json.dump(r, f)

# load json from string
r = json.loads('{"k":"hello"}')

inFile.close()
