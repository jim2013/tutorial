/**
 * Proxy Auto-Configuration(PAC) file for greate fire wall(gfw)
 * 
 * ## Firefox Automatic proxy configuration URL
 * file:///home/song/workspace/tutorial/config/gfw.pac
 * 
 * ## what is pac file
 * A Proxy Auto-configuration(PAC) file is a JavaScript function that determines
 * wether web browser requests(HTTP, HTTPS, and FTP) go directly to the destination
 * or are forwarded to a web proxy server. The JavaScript function contained in 
 * the PAC file defines the function:
 * 
 * function FindProxyForURL(url, host){
 * 	//.....
 * }
 * 
 * ret = FindProxyForURL(url, host);
 * 
 * ## Reference
 * https://developer.mozilla.org/en-US/docs/Web/HTTP/Proxy_servers_and_tunneling/Proxy_Auto-Configuration_(PAC)_file
 */

function FindProxyForURL(url, host)
{
	var debug = true;
	/**
	 * https://stackoverflow.com/questions/1033867/debugging-autoproxy-pac-javascript-with-alert/45610020#45610020
	 * we can use alert to log info, where is alert information?
	 * 1. In FireFox:Tools -> Web Developer -> Browser Console (Ctrl+Shift+J) [This is not Web Console!!]
	 * 2. In Chrome: Go to chrome://net-internals/#events -> Search for a record with description: PAC_JAVASCRIPT_ALERT
     */
	if (debug)
		alert('------proxy url:' + url + ' host:' + host);
	var proxy = 'SOCKS5 127.0.0.1:1080'
	bhosts=[
		'*duckgo.com',
		'*duckduckgo.com',
		/* biswap */
		'squid-nft.io',
		'api.debank.com',
		'cdn.lr-ingest.io',
		'static.hotjar.com',
        /*stackoverflow */
        '*stackoverflow.com',
        '*sstatic.net',
        '*imgur.com',
        '*stackexchange.com',
		/*github */
		'github.com',
		'*.github.com',
		'*githubassets.com',
		'*githubusercontent.com',
		'*github.io',
        /*python*/
        '*python.org',
        '*pypi.org',
		/*google family*/
		'*google.com',
		'*googleapis.com',
		'*googlevideo.com',
		'*ytimg.com',
		'*ggpht.com',
		'*youtube.com',
		'youtu.be',
		'*gstatic.com',
		'*golang.org',
		'*android.com',
		/* download youtube video */
		'*.y2mate.com',
		/*twitter family*/
		'*twitter.com',
		'*t.co',
		'*bit.ly',
		'*twimg.com',
		'*cms-twdigitalassets.com',
		'*twitch.tv',
		'*twitchcdn.net',
		'*.ttvnw.net',
		/*cambridge dictionary*/
		'*.cambridge.org',
		'geolocation.onetrust.com',
		'cdn.ampproject.org',
		/*wiki*/
		'*wikipedia.org',
		'*wikimedia.org',
		/* mit ocw (not be banned but slowly) */
		'ocw.mit.edu',
		/*south china morning post*/
		'*scmp.com',
		/**quora family*/
		'*quora.com',
		'*quoracdn.net',
		/*reddit*/
		'*reddit.com',
		'*redditmedia.com',
		'*redditstatic.com',
		'*redd.it',
		/*facebook*/
		'*facebook.com',
		'*medium.com',
		'*bbc.com',
		/*telegram family*/
		't.me',
		'*telegram.org',
		/*matrix family*/
		'*matrix.org',
		/*askubuntu*/
		'*askubuntu.com',
		/*bloomberg*/
		'*bloomberg.com',
		/*android*/
		'*.android.com',
		/*inoreader*/
		'*.inoreader.com',
		/*feedburner*/
		'*feedburner.com',
		/*shadowsocks*/
		'*shadowsocks.org',
		/*tor*/
		'*.torproject.org',
		/*mysql*/
		'*.mysql.com',
		/*ipfs.io*/
		'*ipfs.io',
		/* nano */
		'mynano.ninja',
		'ws.powernode.cc',
		'api.coingecko.com',
		/*binance*/
		'*binance.com',
		'*binance.org',
		'*bnbstatic.com',
		'*binance.gg',
		'bsc-dataseed1.defibit.io',
		'cdn.cookielaw.org',
		'coinmarketcap.com',
		'*.coinmarketcap.com',
		'*.biswap.org',
		'cdn.onesignal.com',
		'biswap.org',
		'pancakeswap.finance',
		'*.pancakeswap.com',
		'*.herocat.io',
		/* tron */
		'just.network',
		/* polygon */
		'polygonscan.com',
		/* cryptostackers */
		'unpkg.com',
		'kit.fontawesome.com',
		'notify.bubble.is',
		'*.cloudfront.net',
		'cryptostackers.pro',
		/* amazon */
		'*.amazonaws.com',
		/* discord*/
		'discord.com',
		'*.discord.com',
		'*.discordapp.net',
		'*.discordapp.com',
		'gateway.discordapp.gg',
		'gateway.discord.gg',
		/* wireguard */
		'www.wireguard.com',
		/* mysterium */
		'*.mysterium.network',
		'mysterium.network',
		/* substack */
		'*.substack.com',
		'substack.com',
		/* qt */
		'*.qt.io',
		/* mongo */
		'*.mongodb.com',
		/* others */
		'cdn.jsdelivr.net',
		/* zendesk */
		'*.zendesk.com',
		'*ycombinator.com'
	]
	for (var i = 0; i < bhosts.length; i++){
		/**
		 * shExpMatch(str, shexp)
         * shexp is shell glob expression, not regular expressions. * and ? are always supported
		 * however regular expressions are supported by RegExp
		 */
		if (shExpMatch(host, bhosts[i])){
			if (debug)
				alert('+++++proxy url:' + url + ' host:' + host);
			return proxy;
		}
	}
	return 'DIRECT';
}

