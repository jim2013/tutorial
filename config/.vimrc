set nu
set ts=4
set shiftwidth=4
" set tags=./tags,/home/song/Projects/matlab/OpenTLD/tags
set fileencodings=ucs-bom,utf-8,cp936,gb18030,big5,euc-jp,euc-kr,latin1

set ignorecase
set smartcase

" Specify a directory for plugins
" - For Neovim: ~/.local/share/nvim/plugged
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Shorthand notation; fetches https://github.com/iamcco/markdown-preview.vim
Plug 'iamcco/markdown-preview.vim'
Plug 'iamcco/mathjax-support-for-mkdp'
" Plug 'plasticboy/vim-markdown' "syntax highlighting for markdown
Plug 'morhetz/gruvbox'	" gruvbox color scheme

" Initialize plugin system
call plug#end()

" python tab settings
" http://www.vex.net/~x/python_and_vim.html
" autocmd FileType python set tabstop=4|set shiftwidth=4|set expandtab
" ai=autoindent sw=shiftwidth ts=tabstop sta=smarttab et=expandtab
autocmd BufEnter *.py set ai sw=4 ts=4 sta et fo=croql
set softtabstop=4 " makes the spaces feel like real tabs 

" vim-markdown plugin:LaTex math syntax extension on
let g:vim_markdown_math = 1

"color desert
colorscheme gruvbox
set background=dark

" vim automatic wrapping at 80 characters or keeping lines to less than 80
" characters
" set tw=80
" set textwidth=80
set colorcolumn=80
" set colorcolumn=+1

" vimdiff ignore white space
set diffopt+=iwhite
