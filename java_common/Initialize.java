public class Initialize {
	public static int x = A.f1();

	public static void main(String[] args){
		System.out.printf("class Initialize::main() is called, x=%d, A.y=%d\n",
			   Initialize.x, A.y);
	}

	public static int f2() {
		return x + 1;
	}
}

class A {
	public static int y = Initialize.f2();
	public static int f1(){
		System.out.printf("class A::f1() is called, Initialize.x=%d, A.y=%d\n",
			   	Initialize.x, A.y);
		return 100;
	}
}
