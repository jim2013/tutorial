package song;

import java.util.Iterator;
import java.util.ServiceLoader;

import song.IHelloSPIService;

/**
 * Hello world Example for Java Service Provider Interface(SPI)
 *
 * # compile
 * 1. compile IHelloSPIService: javac song/IHelloSPIService
 * 2. compile HelloSPI: javac song/HelloSPI.java 
 * 3. make demo to a jar file:
 *    jar -cvMf demo.jar song/HelloSPI.class song/IHelloSPIService.class
 * 
 * run without any implementation: 
 * java -cp demo.jar song.HelloSPI
 * output is only:
 * done
 *
 * ## add one implementation: HelloSPIService1
 * 1. edit HelloSPIService1.java and META-INF/song.IHelloSPIService
 * the filename of song.IHelloSPIService is the spi interface name, the content
 * of song.IHelloSPIService is the full class name of the implementation :
 * song.HelloService1
 * 2. compile HelloSPIService1: javac song/HelloSPIService1.java
 * 3. make to a jar file
 * jar -cvMf service.jar song/IHelloSPIService.class song/HelloSPIService1.class META-INF/song.IHelloSPIService
 * 4. run with the implementation
 * java -cp demo.jar:service.jar song.HelloSPI
 * output is :
 * hello from HelloSPIService1!
 * done
 *
 * ## add another implementation: HelloSPIService2
 * 1. edit HelloSPIService1.java and META-INF/song.IHelloSPIService
 * the filename of song.IHelloSPIService is the spi interface name, the content
 * of song.IHelloSPIService is the full class name of the two implementation :
 * song.HelloService1
 * song.HelloService2
 * 2. compile HelloSPIService2: javac song/HelloSPIService2.java
 * 3. make the two implementation in a jar file
 * jar -cvMf service.jar META-INF/services/song.IHelloSPIService song/IHelloSPIService.class song/HelloSPIService1.class song/HelloSPIService2.class
 * 4. run with the implementation
 * java -cp demo.jar:service.jar song.HelloSPI
 * output is :
 * hello from HelloSPIService1!
 * hello from HelloSPIService2!
 * done
 *
 */
public class HelloSPI
{
	public static void main(String[] args)
	{
		ServiceLoader<IHelloSPIService> loader = 
			ServiceLoader.load(IHelloSPIService.class);
		Iterator<IHelloSPIService> iter = loader.iterator();
		while(iter.hasNext())
		{
			iter.next().sayHello();
		}
		System.out.println("done");
	}
}
