import java.lang.annotation.Annotation;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.Method;
import java.lang.reflect.Field;

public class AnnotationDemo
{
	/* define an annotation, @retention is necessary, otherwise
	 * we will get NullPointException when invoke brand() method */
	@Retention(RetentionPolicy.RUNTIME)
	@interface A
	{
		String brand() default "unknown";
	}

	@A(brand="Lucky")
	class Bus
	{
		public void 
		drive()
		{
			System.out.println("drive the bus!");
		}
	}

	public static void
	main(String[] args)
	{
		Class<Bus> c = Bus.class;
		A a = c.getAnnotation(A.class);
		System.out.println("brand is " + a.brand());
	}
}
