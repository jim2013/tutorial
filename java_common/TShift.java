public class TBitwise {
	public static void main(String[] argv){
		//0b表示二进制表示整数，类似0x开头表示十六进制
		int i = 0b1000_1000_1111_1010_1111_1000_1111_1010;;
		System.out.println(Integer.toBinaryString(i));
		System.out.println(Integer.toBinaryString(i >> 1));	//有符号右移， 保持符号位不变
		System.out.println(Integer.toBinaryString(i >> 2));
		System.out.println(Integer.toBinaryString(i >>> 1)); //无符号右移， 最左边补零
		System.out.println(Integer.toBinaryString(i >>> 2));
/**
* output:
* 10001000111110101111100011111010
* 11000100011111010111110001111101
* 11100010001111101011111000111110
* 1000100011111010111110001111101
* 100010001111101011111000111110
*/
	}
}
