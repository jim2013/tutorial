import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/*
 * 代理的目的：在不修改原目标对象的前提下，提供额外的功能操作，扩展目标对象的功能
 *
 * 代理种类：jdk动态代理，cglib库提供的动态代理
 * jdk动态代理有一个限制：目标对象至少要实现一个接口
 *
 * cglib原理：通过字节代码处理框架ASM，来转换字节码并生成新类
 *
 * */
public class ProxyDemo 
{

	public static void main(String[] args)
	{
		Service service = new Service();
		IService proxyService = (IService)Proxy.newProxyInstance(
				service.getClass().getClassLoader(),
				service.getClass().getInterfaces(),
				new InvocationHandler()
				{
					@Override
					public Object invoke(Object proxy, Method method, Object[] args) throws Throwable
					{
						System.out.println("before invoke sayHello()");
						Object ret = method.invoke(service, args);
						System.out.println("after invoke sayHello()");
						return ret;
					}
				}
				);
		proxyService.sayHello();
	}
}

interface IService
{
	void sayHello();
}

class Service implements IService
{
	@Override
	public void sayHello()
	{
		System.out.println("hello proxy");
	}
}
