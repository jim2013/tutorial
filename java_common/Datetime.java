import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.DayOfWeek;
import java.time.format.DateTimeFormatter;
import java.util.Date;
/**
 * ## drawbacks of old date-time API(before java 8)
 * 1. Not thread-safe
 * 2. poor design
 * 3. Difficult time zone handling
 *
 * ## java 8 new date-time api
 * 1. `Local`: Simplified date-time api with no complexity of timezone handling
 * 2. `Zoned`: Specialized date-time api to deal with various timezones
 *
 */

public class Datetime {
	public static void main(String args[]){
		testLocal();
		testZone();
		format();
		formatStr2Mill();
	}

	public static void format() {
        Date now = new Date();
		now.toInstant();
		System.out.println("now is:" + 
				DateTimeFormatter.ofPattern("yyyy年MM月dd日 HH:mm")
			.withZone(ZoneId.systemDefault()).format(now.toInstant())
			);

	}

	public static void formatStr2Mill() {
		String str = "2021-12-01 00:00:00";
        DateTimeFormatter formatter = DateTimeFormatter
			.ofPattern("yyyy-MM-dd HH:mm:ss");
		/* user input is local date time, local datetime just igonres zone*/
		LocalDateTime ldt = LocalDateTime
			.parse("2021-12-01 00:00:00", formatter);
		/* when we wish to get milliseconds since epoch, we must provide a zone*/
		ZonedDateTime zdt = ldt.atZone(ZoneId.systemDefault());
		long mill = zdt.toInstant().toEpochMilli();
		System.out.println(str + " in milliseconds since epoch is :" + mill);
	}
	
	public static void testLocal(){
		LocalDateTime now = LocalDateTime.now();
		System.out.println("current local datetime:" + now);
		/* set day of month */
		System.out.println("current month start:" + now.withDayOfMonth(1));
		/*convert local datetime to zoned datetime*/
		ZonedDateTime zonedNow = now.atZone(ZoneId.systemDefault());
		System.out.println("current zoned datetime:" + zonedNow);
	}

	public static void testZone(){
		ZoneId currentZone = ZoneId.systemDefault();
		System.out.println("current zone:" + currentZone);
		ZonedDateTime now = ZonedDateTime.now();
		System.out.println("current local datetime:" + now);
		System.out.println("current epoch milliseconds:" + now.toInstant().toEpochMilli());
		System.out.println("current month start:" + now.withDayOfMonth(1));
		System.out.println("current week start:" + now.with(DayOfWeek.MONDAY));
	}

	/**
	 * In China, Monday is the first day of week
	 * In US, Sunday is the first day of week
	 * https://stackoverflow.com/questions/28450720/get-date-of-first-day-of-week-based-on-localdate-now-in-java-8?noredirect=1
	 */
	public static void firstDayOfWeek(){
		/*
		LocalDate now = LocalDate.now();
		TemporalField fieldISO = WeekFields.of(Locale.FRANCE).dayOfWeek();
		System.out.println(now.with(fieldISO, 1)); // 2015-02-09 (Monday)

		TemporalField fieldUS = WeekFields.of(Locale.US).dayOfWeek();
		System.out.println(now.with(fieldUS, 1)); // 2015-02-08 (Sunday)
		LocalDate ld = LocalDate.of(2017, 8, 18); // Friday as original date

		System.out.println(ld.with(DayOfWeek.SUNDAY)); // 2017-08-20 (2 days later according to ISO)

		// Now let's again set the date to Sunday, but this time in a localized way...
		// the method dayOfWeek() uses localized numbering (Sunday = 1 in US and = 7 in France)

		System.out.println(ld.with(WeekFields.of(Locale.US).dayOfWeek(), 1L)); // 2017-08-13
		System.out.println(ld.with(WeekFields.of(Locale.FRANCE).dayOfWeek(), 7L)); // 2017-08-20
		*/
	}
}
