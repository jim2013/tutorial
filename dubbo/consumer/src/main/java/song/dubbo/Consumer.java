package song.dubbo;

import song.dubbo.DemoService;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Consumer {
	public static void main(String [] args) {
		System.out.println("start to load consumer...");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("consumer.xml");
		context.start();
		System.out.println("finish loading consumer..");
		
		DemoService service = (DemoService)context.getBean("demoService");
		System.out.println(service.sayHello("jim"));
	}

}
