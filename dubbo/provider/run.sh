#!/bin/bash

if [ $# > 0 ]; then
	mvn compile
fi
mvn exec:java -Dexec.mainClass=song.dubbo.Provider
