package song.dubbo;

import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Provider {
	public static void main(String[] args) throws Exception {
		System.out.println("begin to load provider...");
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"provider.xml"});
		context.start();
		System.out.println("finish loading provider.");
		System.in.read();
	}
}
